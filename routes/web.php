<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Belum Callback
Route::any('/Master/PelangganPB/', 'Master\PelangganController@indexPb');
Route::any('/Master/PelangganPD/', 'Master\PelangganController@indexPd');
Route::get('/Master/{id}/delete', 'Master\PelangganController@delete');


//Sudah Callback
Route::any('/sudah-callback-pb', 'Master\PelangganController@indexSudahPb');
Route::any('/sudah-callback-pd', 'Master\PelangganController@indexSudahPd');

//Rekapitulasi
Route::any('/RekapKpPb', 'Master\RekapController@RekapKppb');
Route::any('/RekapIntPb', 'Master\RekapController@RekapIntpb');
Route::any('/RekapHTSPb', 'Master\RekapController@RekapHTSpb');
Route::any('/RekapKlPb', 'Master\RekapController@RekapKlpb');
Route::any('/RekapResPb', 'Master\RekapController@RekapRespb');
Route::any('/RekapATBPb', 'Master\RekapController@RekapATBpb');
Route::any('/RekapPBNPb', 'Master\RekapController@RekapPBNpb');

Route::any('/RekapKpPd', 'Master\RekapController@RekapKppd');
Route::any('/RekapIntPd', 'Master\RekapController@RekapIntpd');
Route::any('/RekapHTSPd', 'Master\RekapController@RekapHTSpd');
Route::any('/RekapKlPd', 'Master\RekapController@RekapKlpd');
Route::any('/RekapResPd', 'Master\RekapController@RekapRespd');
Route::any('/RekapATBPd', 'Master\RekapController@RekapATBpd');
Route::any('/RekapPBNPd', 'Master\RekapController@RekapPBNpd');

Route::any('/RekapKp', 'Master\RekapController@RekapKp');

Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/upload', 'Master\PelangganController@upload');
Route::post('/save', 'Master\PelangganController@save');
Route::get('/action/{id}', 'Master\PelangganController@action');
Route::post('/save/survey/{id}', 'Master\PelangganController@saveSurvey');

//Master
Route::resource('/Master/User', 'Master\UserController');
Route::resource('/Master/Pelanggan', 'Master\PelangganController');
Route::resource('/Master/Target', 'Master\TargetController');
Route::post('/Master/Target/save/{id}', 'Master\TargetController@save');
Route::post('import', 'Master\PelangganController@import')->name('import'); // Route Import DIL
