<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Callback;
use App\Models\Survey;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = new Callback();
        $total = $data->count();
        $belum = $data->where('status', 0)->count();
        $sudah = $data->where('status', 1)->count();
        $today =Survey::whereDate('created_at', Carbon::today())->count();
        $cards= Survey::whereDate('created_at', Carbon::today())->orderBy('updated_at', 'desc')->limit(20)->get();

        $tigapb=Survey::where('ans4','1 hari s/d 3 hari kerja')->whereHas('Callback', function ($query) {
            $query->where('jenistransaksi', '=', 'PASANG BARU');
        })->count();
        $sepuluhpb=Survey::where('ans4', '4 hari s/d 10 hari kerja')->whereHas('Callback', function ($query) {
            $query->where('jenistransaksi', '=', 'PASANG BARU');
        })->count();
        $lebihpb=Survey::where('ans4', 'Lebih dari 10 hari kerja')->whereHas('Callback', function ($query) {
            $query->where('jenistransaksi', '=', 'PASANG BARU');
        })->count();
        $belumpb=Survey::where('ans4', 'Belum menyala')->whereHas('Callback', function ($query) {
            $query->where('jenistransaksi', '=', 'PASANG BARU');
        })->count();

        $tigapd=Survey::where('ans4','1 hari s/d 3 hari kerja')->whereHas('Callback', function ($query) {
            $query->where('jenistransaksi', '=', 'PERUBAHAN DAYA');
        })->count();
        $sepuluhpd=Survey::where('ans4', '4 hari s/d 10 hari kerja')->whereHas('Callback', function ($query) {
            $query->where('jenistransaksi', '=', 'PERUBAHAN DAYA');
        })->count();
        $lebihpd=Survey::where('ans4', 'Lebih dari 10 hari kerja')->whereHas('Callback', function ($query) {
            $query->where('jenistransaksi', '=', 'PERUBAHAN DAYA');
        })->count();
        $belumpd=Survey::where('ans4', 'Belum menyala')->whereHas('Callback', function ($query) {
            $query->where('jenistransaksi', '=', 'PERUBAHAN DAYA');
        })->count();
        
        return view('home',compact('data', 'total', 'belum', 'sudah', 'today', 'cards', 'tigapb', 'sepuluhpb', 'lebihpb', 'belumpb', 'tigapd', 'sepuluhpd', 'lebihpd', 'belumpd'));
    }
}
