<?php

namespace App\Http\Controllers\Master;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Auth\Role;
// use App\Models\Auth\Role_user;
use Session;
use Excel;
use File;

class UserController  extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

     public function create()
    {
        $role=Role::get();
        return view('Master/User.create', compact('role'));
    }

     public function store(Request $request)
    {
        $input = $this->validate(request(), [
           'name' => 'required',
           'email' => 'required|string|email|max:255|unique:users',
           'password' => 'required|string|min:6|confirmed',
           'role' => 'required'
        ]);

        $User=User::create([
          'name' => $request['name'],
          'email' => $request['email'],
          'password' => bcrypt($request['password']),
          'id_rayon' => $request['rayon'],
          'id_area' => $request['area'],
          'role_user_id' => $request['role']
        ])->assignRole($request['role']);

        return redirect('Master/User')->with('success','User telah ditambahkan');
    }

     public function index()
    {
        $user = User::get();
        return view('Master/User.index', compact('user'));
    }

    public function filter(Request $request)
    {
         return view('Master/User.index');
    }

     public function edit($id)
    {
        $user = User::find($id);
        $role=Role::get();
        return view('Master/User.edit', compact('user', 'role'));
    }

     public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');

        // $role = Role_user::find($user->RoleUser->id);
        $role->role_id = $request->get('role');
        
        $user->save();
        $role->save();

       return redirect('Master/User')->with('success','User telah di ubah');
    }
  
    public function show($id)
    {
        $user = User::find($id);
         return view('Master/User.show', compact('user'));
    }

     public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('Master/User')->with('success','User telah di hapus');
    }
}
