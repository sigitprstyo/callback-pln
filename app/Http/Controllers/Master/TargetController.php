<?php

namespace App\Http\Controllers\Master;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Settings;

class TargetController  extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

     public function index()
    {
        $settings = Settings::first();
        return view('Master/Target.index', compact('settings'));
    }

     public function save(Request $request, $id)
    {
        $settings = Settings::find($id);
        $settings->target_callback = $request->get('target_callback');
        
        $settings->save();

       return redirect('Master/Target')->with('success','settings telah di ubah');
    }

}
