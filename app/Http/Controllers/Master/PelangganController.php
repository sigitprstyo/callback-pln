<?php

namespace App\Http\Controllers\Master;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Callback;
use App\Models\Survey;
use App\Models\MUnit;
use Illuminate\Support\Facades\DB;
use Session;
use Excel;
use File;

class PelangganController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

     public function upload()
    {
         return view('Callback.create');
    }

    public function save(Request $request)
    {
        //validate the xls file
        $this->validate($request, array(
            'file_pelanggan'      => 'required'
        ));
 
        if($request->hasFile('file_pelanggan')){
            $extension = File::extension($request->file_pelanggan->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
 
                $path = $request->file_pelanggan->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();

                $count=1;
                if(!empty($data) && $data->count()){

                    foreach ($data as $key => $value) { 
                        if (Callback::where('noregister', $value->noregister)->first()) {
                            continue;
                        }
                        if ($value->noregister=='') {
                            continue;
                        }
                        $insert = Callback::create([
                        'unitupi' => $value->unitupi,
                        'unitap' => $value->unitap,
                        'unitup' => $value->unitup,
                        'tglmohon' => $value->tglmohon,
                        'noagenda' => $value->noagenda,  
                        'noregister' => $value->noregister,
                        'idpel' => $value->idpel, 
                        'nama' => $value->nama,
                        'namapemohon' => $value->namapemohon,
                        'alamat' => $value->alamat, 
                        'alamatpemohon' => $value->alamatpemohon,
                        'koordinat_x' => $value->koordinat_x,
                        'koordinat_y' => $value->koordinat_y,    
                        'notelp' => $value->notelp,
                        'nohp' => $value->nohp,
                        'jenistransaksi' => $value->jenistransaksi,
                        'tariflama' => $value->tariflama,
                        'dayalama' => $value->dayalama,
                        'tarif' => $value->tarif, 
                        'daya' => $value->daya,
                        'asalmohon' => $value->asalmohon,
                        'jenismk'  => $value->jenismk,
                        'rpujl' => $value->rpujl,
                        'rpbp' => $value->rpbp,
                        'rpstroomawal' => $value->rpstroom, 
                        'jenisbayar' => $value->jenisbayar,
                        'tglbayar' => $value->tglbayar,
                        'statuspermohonan' => $value->statuspermohonan,
                        'statuspdl' => $value->statuspdl,
                        'tglsip' => $value->tglsip,
                        'tglpk' => $value->tglpk, 
                        'unitpk' => $value->unitpk,
                        'tglperemajaan' => $value->tglperemajaan,
                        'tglrubahmk' => $value->tglrubahmk,
                        'thblmut' => $value->thblmut,
                        'tglpdl' => $value->tglpdl,
                        'nomorpdl' => $value->nomorpdl, 
                        'tglpenangguhan' => $value->tglpenangguhan,
                        'tglsiapsambung' => $value->tglsiapsambung,
                        'tglrestitusi' => $value->tglrestitusi,
                        'tglbptitipan' => $value->tglbptitipan,
                        'tglpengesahan' => $value->tglpengesahan,
                        'durasiharikerja' => $value->durasiharikerja, 
                        'durasiharikalender' => $value->durasiharikalender,
                        'kriteriatmp' => $value->kriteriatmp,
                        'alasankriteriatmp' => $value->alasankriteriatmp,
                        'keterangankriteriatmp' => $value->keterangankriteriatmp,
                        'estimasidurasi' => $value->estimasidurasi,
                        'statuspenangguhan' => $value->statuspenangguhan, 
                        'alasanpenangguhan' => $value->alasanpenangguhan,
                        'keteranganalasanpenangguhan' => $value->keterangankriteriatmp,
                        'statusrestitusi' => $value->statusrestitusi,
                        'created_by' => Auth::user()->id,
                        ]);
                        if ($insert) {
                           $count++;
                        }

                    }

                    // if(!empty($insert)){
                        // $truncate = CallBack::query()->truncate();
                        // $insertData = CallBack::insert($insert);
                        // if ($insertData) {
                        Session::flash('success', 'Your '.$count.' Data has successfully imported');
                        // }else {                        
                            // Session::flash('error', 'Error inserting the data..');
                            // return back();
                        // }
                    // }
                // }
 
                return back();
 
            }else {
                Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
                return back();
            }
        }
        }
        
    }

    public function action($id)
    {
        $pelanggan= Callback::find($id);
        if ($pelanggan->status=='1') {
            return redirect('Master/Pelanggan')->with('errors','Survey telah dilakukan');
        }
        return view('Callback.action', compact('pelanggan'));
    }

    public function saveSurvey(Request $request, $id)
    {
        Survey::create([
          'callback_id' => $id,
          'ans1' => $request['ans1'],
          'ans1' => $request['ans1'],
          'ans2' => $request['ans2'],
          'ans3' => $request['ans3'],
          'ans4' => $request['ans4'],
          'ans5' => $request['ans5'],
          'ans6' => $request['ans6'],
          'ans7' => $request['ans7'],
          'ans8' => $request['ans8'],
          'notelp' => $request['notelp'],
          'kritiksaran' => $request['kritiksaran'],
          'action_by' => Auth::user()->id,
        ]);

        Callback::where('id', $id)->update(['status' => 1]);

        return redirect('Master/Pelanggan')->with('success','Survey telah disimpan');
    }

// Index
     public function index()
    {
        $cards=CallBack::where('status', 0)->inRandomOrder()->paginate(50);
        return view('Callback.index', compact('cards'));  
    }
    public function indexPB(Request $request)
    {
        $url = '/Master/PelangganPB/';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards=CallBack::where([['status', 0],['jenistransaksi', 'PASANG BARU']])->whereMonth('created_at', $month)->whereYear('created_at', $year)->inRandomOrder()->paginate(50);
        return view('Callback.index', compact('cards', 'url'));
    }
    public function indexPD(Request $request)
    {
        $url = '/Master/PelangganPD/';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards=CallBack::where([['status', 0],['jenistransaksi', 'PERUBAHAN DAYA']])->whereMonth('created_at', $month)->whereYear('created_at', $year)->inRandomOrder()->paginate(50);
        return view('Callback.index', compact('cards', 'url'));  
    }

    public function indexSudahPb(Request $request)
    {
        $url = '/sudah-callback-pb';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards=CallBack::join('survey_result', 'callback.id', '=', 'survey_result.callback_id')->where([['callback.status', 1],['callback.jenistransaksi', 'PASANG BARU']])->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->orderBy('survey_result.id','desc')->paginate(50);
        return view('Callback.sudah', compact('cards', 'url'));  
    }
    public function indexSudahPd(Request $request)
    {
        $url = '/sudah-callback-pd';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards=CallBack::join('survey_result', 'callback.id', '=', 'survey_result.callback_id')->where([['callback.status', 1],['callback.jenistransaksi', 'PERUBAHAN DAYA']])->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->orderBy('survey_result.id','desc')->paginate(50);
        return view('Callback.sudah', compact('cards', 'url'));  
    }

// Rekapitulasi

    public function RekapKp(Request $request)
    {
        $url = '/sudah-callback-pb';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = '1 hari s/d 3 hari kerja' THEN 1 END) as tiga"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = '4 hari s/d 10 hari kerja' THEN 1 END) as sepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Lebih dari 10 hari kerja' THEN 1 END) as lebihsepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where('status', 1)->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekap', compact('cards'));  
    }

    public function RekapInt(Request $request)
    {
        $url = '/RekapKp';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = '1 hari s/d 3 hari kerja' THEN 1 END) as tiga"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = '4 hari s/d 10 hari kerja' THEN 1 END) as sepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Lebih dari 10 hari kerja' THEN 1 END) as lebihsepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where('status', 1)->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekapInt', compact('cards', 'url'));  
    }

    public function RekapHTS(Request $request)
    {
        $url = '/RekapKp';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = '1 hari s/d 3 hari kerja' THEN 1 END) as tiga"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = '4 hari s/d 10 hari kerja' THEN 1 END) as sepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Lebih dari 10 hari kerja' THEN 1 END) as lebihsepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where('status', 1)->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekapInt', compact('cards'));  
    }

    public function RekapKl(Request $request)
    {
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = '1 hari s/d 3 hari kerja' THEN 1 END) as tiga"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = '4 hari s/d 10 hari kerja' THEN 1 END) as sepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Lebih dari 10 hari kerja' THEN 1 END) as lebihsepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where('status', 1)->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekap', compact('cards'));  
    }

    public function RekapRes(Request $request)
    {
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = '1 hari s/d 3 hari kerja' THEN 1 END) as tiga"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = '4 hari s/d 10 hari kerja' THEN 1 END) as sepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Lebih dari 10 hari kerja' THEN 1 END) as lebihsepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where('status', 1)->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekapInt', compact('cards'));  
    }


    public function RekapPBN(Request $request)
    {
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where('status', 1)->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekapPBN', compact('cards'));  
    }

    public function RekapATB(Request $request)
    {
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans5 = 'Ya, memberi biaya tambahan' THEN 1 END) as ya"),
             \DB::raw("count(CASE WHEN survey_result.ans5 = 'Tidak mengeluarkan biaya tambahan lainnya' THEN 1 END) as tidak"),
            \DB::raw("count(*) as total"))->where('status', 1)->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekapAtb', compact('cards'));
    }

    public function filter(Request $request)
    {
         return view('Callback.index');
    }

     public function edit($id)
    {
        $card = CallBack::find($id);
        return view('Callback.edit', compact('card'));
    }

     public function update(Request $request, $id)
    {
        $card = CallBack::find($id);
        $card->id_card_code = $request->get('id_card_code');
        $card->card_number = $request->get('card_number');
        $card->valid_date = $request->get('valid_date');
        $card->active = $request->get('active');
        $card->updated_by = Auth::user()->id;

        $card->save();
       return redirect('Master/Pelanggan')->with('success','Data telah di ubah');
    }

    public function show($id)
    {
        $card = CallBack::find($id);
         return view('Callback.show', compact('card'));
    }

     public function delete($id)
    {
        $card = CallBack::find($id);
        $card->delete();
        return redirect('Master/Pelanggan')->with('success','Kode Transaksi telah di hapus');
    }
}
