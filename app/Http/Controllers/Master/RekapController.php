<?php

namespace App\Http\Controllers\Master;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Callback;
use App\Models\Survey;
use App\Models\MUnit;
use App\Models\Settings;
use App\User;
use Illuminate\Support\Facades\DB;

class RekapController extends Controller
{
// Rekapitulasi

    public function RekapKppb(Request $request)
    {
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = '1 hari s/d 3 hari kerja' THEN 1 END) as tiga"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = '4 hari s/d 10 hari kerja' THEN 1 END) as sepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Lebih dari 10 hari kerja' THEN 1 END) as lebihsepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where([['status', 1],['jenistransaksi', 'PASANG BARU']])->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekap', compact('cards', 'url'));  
    }

    public function RekapIntpb(Request $request)
    {
        $url = '/RekapIntPb';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = '1 hari s/d 3 hari kerja' THEN 1 END) as tiga"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = '4 hari s/d 10 hari kerja' THEN 1 END) as sepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Lebih dari 10 hari kerja' THEN 1 END) as lebihsepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where([['status', 1],['jenistransaksi', 'PASANG BARU']])->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekapInt', compact('cards', 'url'));  
    }

    public function RekapHTSpb(Request $request)
    {
        $url = '/RekapHTSPb';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = '1 hari s/d 3 hari kerja' THEN 1 END) as tiga"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = '4 hari s/d 10 hari kerja' THEN 1 END) as sepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Lebih dari 10 hari kerja' THEN 1 END) as lebihsepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where([['status', 1],['jenistransaksi', 'PASANG BARU']])->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekapInt', compact('cards', 'url'));  
    }

    public function RekapKlpb(Request $request)
    {
        $url = '/RekapKlPb';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = '1 hari s/d 3 hari kerja' THEN 1 END) as tiga"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = '4 hari s/d 10 hari kerja' THEN 1 END) as sepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Lebih dari 10 hari kerja' THEN 1 END) as lebihsepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where([['status', 1],['jenistransaksi', 'PASANG BARU']])->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekap', compact('cards', 'url'));  
    }

    public function RekapRespb(Request $request)
    {
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = '1 hari s/d 3 hari kerja' THEN 1 END) as tiga"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = '4 hari s/d 10 hari kerja' THEN 1 END) as sepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Lebih dari 10 hari kerja' THEN 1 END) as lebihsepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where([['status', 1],['jenistransaksi', 'PASANG BARU']])->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekapInt', compact('cards', 'url'));  
    }


    public function RekapPBNpb(Request $request)
    {
        $url = '/RekapPBNPd';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where([['status', 1],['jenistransaksi', 'PASANG BARU']])->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekapPBN', compact('cards', 'url'));  
    }

    public function RekapATBpb(Request $request)
    {
        $url = '/RekapATBPb';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans5 = 'Ya, memberi biaya tambahan' THEN 1 END) as ya"),
             \DB::raw("count(CASE WHEN survey_result.ans5 = 'Tidak mengeluarkan biaya tambahan lainnya' THEN 1 END) as tidak"),
            \DB::raw("count(*) as total"))->where([['status', 1],['jenistransaksi', 'PASANG BARU']])->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekapAtb', compact('cards', 'url'));  
    }

    public function RekapKppd(Request $request)
    {
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = '1 hari s/d 3 hari kerja' THEN 1 END) as tiga"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = '4 hari s/d 10 hari kerja' THEN 1 END) as sepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Lebih dari 10 hari kerja' THEN 1 END) as lebihsepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where([['status', 1],['jenistransaksi', 'PERUBAHAN DAYA']])->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekap', compact('cards', 'url'));  
    }

    public function RekapIntpd(Request $request)
    {
        $url = '/RekapIntPd';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = '1 hari s/d 3 hari kerja' THEN 1 END) as tiga"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = '4 hari s/d 10 hari kerja' THEN 1 END) as sepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Lebih dari 10 hari kerja' THEN 1 END) as lebihsepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where([['status', 1],['jenistransaksi', 'PERUBAHAN DAYA']])->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekapInt', compact('cards', 'url'));  
    }

    public function RekapHTSpd(Request $request)
    {
        $url = '/RekapHTSPd';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = '1 hari s/d 3 hari kerja' THEN 1 END) as tiga"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = '4 hari s/d 10 hari kerja' THEN 1 END) as sepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Lebih dari 10 hari kerja' THEN 1 END) as lebihsepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where([['status', 1],['jenistransaksi', 'PERUBAHAN DAYA']])->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekapInt', compact('cards', 'url'));  
    }

    public function RekapKlpd(Request $request)
    {
        $url = '/RekapKlPd';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = '1 hari s/d 3 hari kerja' THEN 1 END) as tiga"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = '4 hari s/d 10 hari kerja' THEN 1 END) as sepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Lebih dari 10 hari kerja' THEN 1 END) as lebihsepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where([['status', 1],['jenistransaksi', 'PERUBAHAN DAYA']])->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekap', compact('cards', 'url'));  
    }

    public function RekapRespd(Request $request)
    {
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = '1 hari s/d 3 hari kerja' THEN 1 END) as tiga"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = '4 hari s/d 10 hari kerja' THEN 1 END) as sepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Lebih dari 10 hari kerja' THEN 1 END) as lebihsepuluh"),
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where([['status', 1],['jenistransaksi', 'PERUBAHAN DAYA']])->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekapInt', compact('cards', 'url'));  
    }


    public function RekapPBNpd(Request $request)
    {
        $url = '/RekapPBNPd';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans4 = 'Belum menyala' THEN 1 END) as belum"),
            \DB::raw("count(*) as total"))->where([['status', 1],['jenistransaksi', 'PERUBAHAN DAYA']])->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekapPBN', compact('cards', 'url'));  
    }

    public function RekapATBpd(Request $request)
    {
        $url = '/RekapATBPd';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
        $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('m_unit', 'callback.unitup', '=', 'm_unit.id_rayon')
        ->select('m_unit.nama_rayon', 
             \DB::raw("count(CASE WHEN survey_result.ans5 = 'Ya, memberi biaya tambahan' THEN 1 END) as ya"),
             \DB::raw("count(CASE WHEN survey_result.ans5 = 'Tidak mengeluarkan biaya tambahan lainnya' THEN 1 END) as tidak"),
            \DB::raw("count(*) as total"))->where([['status', 1],['jenistransaksi', 'PERUBAHAN DAYA']])->groupBy('m_unit.nama_rayon')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        return view('Rekap.rekapAtb', compact('cards', 'url'));  
    }

    public function RekapKp(Request $request)
    {
        $url = '/RekapKp';
        $month=$request['month'];
        $year=$request['year'];
        $month= $month==null ? date('m') : $month;
        $year= $year==null ? date('Y') : $year;
         $cards= DB::table('survey_result')->join('callback', 'survey_result.callback_id', '=', 'callback.id')->join('users', 'survey_result.action_by', '=', 'users.id')
        ->select(
            'users.name as name',
            \DB::raw("count(CASE WHEN callback.jenistransaksi = 'PASANG BARU' THEN 1 END) as pb"),
            \DB::raw("count(CASE WHEN callback.jenistransaksi = 'PERUBAHAN DAYA' THEN 1 END) as pd"),
            \DB::raw("count(*) as total"))->where('callback.status', 1)->groupBy('users.name')->whereMonth('survey_result.created_at', $month)->whereYear('survey_result.created_at', $year)->get();

        $target= Settings::first();

        return view('Rekap.rekapKp', compact('cards', 'target'));  
    }
}
