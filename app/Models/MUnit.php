<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MUnit extends Model
{
    protected $table = "m_unit";

    protected $fillable = [
        'id_rayon',
        'nama_rayon',
        'id_area',
        'nama_area',
        'id_wilayah',  
        'nama_wilayah'
    ];
}
