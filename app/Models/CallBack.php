<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CallBack extends Model
{
    protected $table = "callback";
    public $timestamps = true;

    protected $fillable = [
        'unitupi',
        'unitap',
        'unitup',
        'tglmohon',
        'noagenda',  
        'noregister',
        'idpel', 
        'nama',
        'namapemohon',
        'alamat', 
        'alamatpemohon',
        'koordinat_x',
        'koordinat_y', 
        'notelp',
        'nohp',
        'jenistransaksi',
        'tariflama',
        'dayalama',
        'tarif', 
        'daya',
        'asalmohon',
        'jenismk',
        'rpujl',
        'rpbp',
        'rpstroom', 
        'jenisbayar',
        'tglbayar',
        'statuspermohonan',
        'statuspdl',
        'tglsip',
        'tglpk', 
        'unitpk',
        'tglperemajaan',
        'tglrubahmk',
        'thblmut',
        'tglpdl',
        'nomorpdl', 
        'tglpenangguhan',
        'tglsiapsambung',
        'tglrestitusi',
        'tglbptitipan',
        'tglpengesahan',
        'durasiharikerja', 
        'durasiharikalender',
        'kriteriatmp',
        'alasankriteriatmp',
        'keterangankriteriatmp',
        'estimasidurasi',
        'statuspenangguhan', 
        'alasanpenangguhan',
        'keteranganalasanpenangguhan',
        'statusrestitusi'
    ];

    public function Survey() {
        return $this->belongsTo('App\Models\Survey', 'id', 'callback_id');
    }
    public function Operator() {
        return $this->belongsTo('App\User','created_by');
    }
    public function Unit() {
        return $this->belongsTo('App\Models\MUnit', 'unitup', 'id_rayon');
    }
}
