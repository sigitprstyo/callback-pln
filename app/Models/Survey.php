<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table = "survey_result";

    protected $fillable = [
        'callback_id',
        'ans1',
        'ans2',
        'ans3',
        'ans4',  
        'ans5',
        'ans6', 
        'ans7',
        'ans8',
        'result',
        'action_by',
    ];

    public function Callback() {
        return $this->belongsTo('App\Models\Callback');
    }
}
