<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotelpSaranToSurveyResultTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('survey_result', function (Blueprint $table) {
            $table->string('notelp')->nullable();
            $table->string('kritiksran')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('survey_result', function (Blueprint $table) {
            $table->dropColumn('notelp');
            $table->dropColumn('kritiksran');
        });
    }
}
