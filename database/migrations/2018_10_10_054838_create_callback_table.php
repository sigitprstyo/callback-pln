<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCallBackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('callback', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('unitupi')->unsigned();
            $table->integer('unitap')->unsigned();
            $table->integer('unitup')->unsigned();
            $table->date('tglmohon');
            $table->string('noagenda');
            $table->string('noregister')->unique();
            $table->string('idpel')->unique();
            $table->string('nama');
            $table->string('namapemohon');
            $table->string('alamat')->nullable();  
            $table->string('alamatpemohon')->nullable();
            $table->string('koordinat_x')->nullable();
            $table->string('koordinat_y')->nullable();
            $table->string('notelp'); 
            $table->string('nohp'); 
            $table->string('jenistransaksi'); 
            $table->string('tariflama')->nullable(); 
            $table->string('dayalama')->nullable(); 
            $table->string('tarif')->nullable();
            $table->string('daya')->nullable(); 
            $table->string('asalmohon')->nullable(); 
            $table->string('jenismk');
            $table->string('rpujl');
            $table->string('rpbp');
            $table->string('rpstroomawal');
            $table->string('jenisbayar');
            $table->date('tglbayar');
            $table->string('statuspermohonan');
            $table->string('statuspdl');
            $table->date('tglsip');
            $table->date('tglpk');
            $table->string('unitpk');
            $table->date('tglperemajaan');
            $table->date('tglrubahmk');
            $table->string('thblmut');
            $table->date('tglpdl');
            $table->string('nomorpdl');
            $table->date('tglpenangguhan')->nullable();
            $table->date('tglsiapsambung')->nullable();
            $table->date('tglrestitusi')->nullable();
            $table->date('tglbptitipan')->nullable();
            $table->date('tglpengesahan');
            $table->integer('durasiharikerja')->unsigned();
            $table->integer('durasiharikalender')->unsigned();
            $table->string('kriteriatmp');
            $table->string('alasankriteriatmp');
            $table->string('keterangankriteriatmp')->nullable();
            $table->string('estimasidurasi');
            $table->string('statuspenangguhan');
            $table->string('alasanpenangguhan')->nullable();
            $table->string('keteranganalasanpenangguhan')->nullable();
            $table->string('statusrestitusi')->nullable();
            $table->tinyInteger('status')->default(0)->unsigned();
            $table->tinyInteger('created_by')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_approval');
    }
}
