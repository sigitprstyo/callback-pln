@extends('layouts.app')

@section('content')

<div class="row">
            <div class="col-xs-12 col-md-6 col-lg-3">
                        <div class="widget widget-tile" style="background-color: #373733;color: white; font-weight: bold;">
                          
                          <div class="data-info">
                            <div class="desc">Total Data</div>
                            <div class="value"><span data-toggle="counter" data-end="{{$total}}" class="number">{{$total}}</span></a>
                            </div>
                          </div>
                        </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
                        <div class="widget widget-tile" style="background-color: #ec1c24;color: white; font-weight: bold;">
                          
                          <div class="data-info">
                            <div class="desc">Belum Callback</div>
                            <div class="value"><span data-toggle="counter" data-end="{{$belum}}" class="number">{{$belum}}</span>
                            </div>
                          </div>
                        </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
                        <div class="widget widget-tile" style="background-color: #0ed145;color: white; font-weight: bold;">
                          
                          <div class="data-info">
                            <div class="desc">Sudah Calllback</div>
                            <div class="value"><span data-toggle="counter" data-end="{{$sudah}}" class="number">{{$sudah}}</span>
                            </div>
                          </div>
                        </div>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-3">
                        <div class="widget widget-tile" style="background-color: #00a8f3;color: white; font-weight: bold;">
                          
                          <div class="data-info">
                            <div class="desc">Call Back Hari Ini</div>
                            <div class="value"><span data-toggle="counter" data-end="{{$today}}" class="number">{{$today}}</span>
                            </div>
                          </div>
                        </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="panel panel-default">
                <div class="panel-heading panel-heading-divider">
                 <span class="title">Kecepatan Proses Pelayanan Pasang Baru</span>
                </div>
                <div class="panel-body">
                  <div class="chartdiv" id="chartdivpb"></div>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="panel panel-default">
                <div class="panel-heading panel-heading-divider">
                 <span class="title">Kecepatan Proses Pelayanan Penambahan Daya</span>
                </div>
                <div class="panel-body">
                  <div class="chartdiv" id="chartdivpd"></div>
                </div>
              </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="panel panel-default panel-table">
                <div class="panel-heading">
                  <div class="title">Callback Hari Ini</div>
                </div>
                <div class="panel-body table-responsive">
                  <table class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>No Register</th>
                        <th>ID Pelanggan</th>
                        <th>Nama</th>    
                        <th>Jenis Transaksi</th>
                        <th>Tarif / Daya</th>
                        <th>Asal Mohon</th>
                        <th>Durasi Hari Kerja/Kalender</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      @if (count($cards)>0)
                        @foreach ($cards as $no => $key)
                        <tr>
                          <td>{{ ($no+1) }}</td>
                          <td>{{ $key->Callback->noregister }}</td>
                          <td>{{ $key->Callback->idpel }}</td>
                          <td>{{ $key->Callback->nama }}</td>
                          <td>{{ $key->Callback->jenistransaksi }}</td>
                          <td>{{ $key->Callback->tarif }}/{{ $key->Callback->daya }}</td>
                          <td>{{ $key->Callback->asalmohon }}</td>
                          <td>{{ $key->Callback->durasiharikerja }}/{{ $key->Callback->durasiharikalender }}</td>
                          <td><a href="{{action('Master\PelangganController@show', $key->Callback->id )}}" title="View"><span class="btn btn-sm btn-primary"><i class="mdi mdi-eye"></i></span></a></td>
                            
                        </tr>
                        @endforeach
                        @else
                      <tr>
                        <td colspan="9">No Data</td>
                      </tr>
                      @endif
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
@endsection

@section('afterscript')
<!-- Styles -->
<style>
.chartdiv {
  width: 100%;
  height: 350px;
}

</style>

<!-- Resources -->
<script src="{{ asset('beagle/lib/amcharts/core.js') }}"></script>
<script src="{{ asset('beagle/lib/amcharts/charts.js') }}"></script>
<script src="{{ asset('beagle/lib/amcharts/animated.js') }}"></script>

<!-- Chart code -->
<script>

  var tigapb = '<?php echo $tigapb ;?>';
  var sepuluhpb = '<?php echo $sepuluhpb ;?>';
  var lebihpb = '<?php echo $lebihpb ;?>';
  var belumpb = '<?php echo $belumpb ;?>';

  var tigapd = '<?php echo $tigapd ;?>';
  var sepuluhpd = '<?php echo $sepuluhpd ;?>';
  var lebihpd = '<?php echo $lebihpd ;?>';
  var belumpd = '<?php echo $belumpd ;?>';


// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
var chart = am4core.create("chartdivpb", am4charts.XYChart3D);

// Add data
chart.data = [{
  "status": "1-3 Hari",
  "visits": {{ $tigapb }}
}, {
  "status": "4-10 Hari",
  "visits": {{ $sepuluhpb }}
}, {
  "status": "> 10 Hari",
  "visits": {{ $lebihpb }}
}, {
  "status": "Belum Nyala",
  "visits": {{ $belumpb }}
}];

// Create axes
let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "status";
categoryAxis.renderer.labels.template.rotation = 320;
categoryAxis.renderer.labels.template.hideOversized = false;
categoryAxis.renderer.minGridDistance = 20;
categoryAxis.renderer.labels.template.horizontalCenter = "right";
categoryAxis.renderer.labels.template.verticalCenter = "middle";
categoryAxis.tooltip.label.rotation = 320;
categoryAxis.tooltip.label.horizontalCenter = "right";
categoryAxis.tooltip.label.verticalCenter = "middle";

let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.title.text = "Countries";
valueAxis.title.fontWeight = "bold";

// Create series
var series = chart.series.push(new am4charts.ColumnSeries3D());
series.dataFields.valueY = "visits";
series.dataFields.categoryX = "status";
series.name = "Visits";
series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
series.columns.template.fillOpacity = .8;

var columnTemplate = series.columns.template;
columnTemplate.strokeWidth = 2;
columnTemplate.strokeOpacity = 1;
columnTemplate.stroke = am4core.color("#FFFFFF");

columnTemplate.adapter.add("fill", (fill, target) => {
  return chart.colors.getIndex(target.dataItem.index);
})

columnTemplate.adapter.add("stroke", (stroke, target) => {
  return chart.colors.getIndex(target.dataItem.index);
})

chart.cursor = new am4charts.XYCursor();
chart.cursor.lineX.strokeOpacity = 0;
chart.cursor.lineY.strokeOpacity = 0;


</script>

<!-- Chart code -->
<script>

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
var chartpd = am4core.create("chartdivpd", am4charts.XYChart3D);

// Add data
chartpd.data = [{
  "status": "1-3 Hari",
  "visits": {{ $tigapd }}
}, {
  "status": "4-10 Hari",
  "visits": {{ $sepuluhpd }}
}, {
  "status": "> 10 Hari",
  "visits": {{ $lebihpd }}
}, {
  "status": "Belum Nyala",
  "visits": {{ $belumpd }}
}];

// Create axes
let categoryAxispd = chartpd.xAxes.push(new am4charts.CategoryAxis());
categoryAxispd.dataFields.category = "status";
categoryAxispd.renderer.labels.template.rotation = 320;
categoryAxispd.renderer.labels.template.hideOversized = false;
categoryAxispd.renderer.minGridDistance = 20;
categoryAxispd.renderer.labels.template.horizontalCenter = "right";
categoryAxispd.renderer.labels.template.verticalCenter = "middle";
categoryAxispd.tooltip.label.rotation = 320;
categoryAxispd.tooltip.label.horizontalCenter = "right";
categoryAxispd.tooltip.label.verticalCenter = "middle";

let valueAxispd = chartpd.yAxes.push(new am4charts.ValueAxis());
valueAxispd.title.text = "Countries";
valueAxispd.title.fontWeight = "bold";

// Create series
var series = chartpd.series.push(new am4charts.ColumnSeries3D());
series.dataFields.valueY = "visits";
series.dataFields.categoryX = "status";
series.name = "Visits";
series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
series.columns.template.fillOpacity = .8;

var columnTemplate = series.columns.template;
columnTemplate.strokeWidth = 2;
columnTemplate.strokeOpacity = 1;
columnTemplate.stroke = am4core.color("#FFFFFF");

columnTemplate.adapter.add("fill", (fill, target) => {
  return chartpd.colors.getIndex(target.dataItem.index);
})

columnTemplate.adapter.add("stroke", (stroke, target) => {
  return chartpd.colors.getIndex(target.dataItem.index);
})

chartpd.cursor = new am4charts.XYCursor();
chartpd.cursor.lineX.strokeOpacity = 0;
chartpd.cursor.lineY.strokeOpacity = 0;


</script>
@endsection