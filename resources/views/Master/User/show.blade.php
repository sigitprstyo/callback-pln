@extends('layouts.app')
@section('content')
<h2>Detail SPK Pemeliharaan Gardu </h2>

      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif
      <div class="panel-body">

      <div class="tab-content">
        <div class="tab-pane active" id="tab-1">
        <form method="post" action="{{action('SpkPemeliharaan\SpkPemeliharaanGarduController@update', $id  )}}">
        {{csrf_field()}}
        <input name="_method" type="hidden" value="PATCH">
        <div class="row">
           <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="nama_penyulang">No WO Pemeliharaan:</label>
            <input type="text"  class="form-control" name="wo_number_pemeliharaan" value="{{$work_orders->wo_number_pemeliharaan}}" readonly>
          </div>
        </div>
            <div class="row">
           <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="name">No Laporan Inspeksi:</label>
            <input type="text" class="form-control"  name="id_laporan_inspeksi" value="{{$work_orders->id_laporan_inspeksi}}" readonly>
          </div>
        </div>

      <div class="row">
         <div class="col-lg-12 margin-tb"></div>
        <div class="form-group col-md-3">
            <label for="name">Jenis Pemeliharaan:</label>
             <input type="text" class="form-control" name="jenis_pemeliharaan" value="{{$work_orders->jenis_pemeliharaan}}" readonly>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-4">
          <label class="font-normal">Tanggal</label>
                <div class="input-group date">
                  <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" class="form-control" name="tanggal" value="{{$work_orders->tanggal}}" readonly>
                 </div>
            </div>
          </div>

        <div class="row">
           <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-3">
            <label for="name">Pemadaman:</label>
                   <input type="text" class="form-control" name="pemadaman" value="{{$work_orders->pemadaman}}" readonly>
          </div> 

          <div class="form-group col-md-3">
             <label for="name">Prioritas:</label>
                   <select type="text" class="form-control" name="prioritas" readonly>
                      <option value="{{$work_orders->prioritas}}">{{$work_orders->prioritas}}</option>
                  </select>
          </div>
      </div>

          <div class="row">
           <div class="col-lg-12 margin-tb"></div>
            <div class="form-group col-md-8">
            <label for="name">Uraian Pekerjaan:</label>
            <input type="text" class="form-control" name="uraian_pekerjaan" value="{{$work_orders->uraian_pekerjaan}}" readonly>
          </div>
        </div>  


         <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-8">
            <label for="keterangan">Keterangan :</label>
            <input type="text" class="form-control" name="keterangan" value="{{$work_orders->keterangan}}" readonly>
          </div>
        </div>

        <div class="row">
           <div class="form-group col-md-8">
      <table class="table table-bordered table-hover" id="tab_logic">
        <thead>
          <tr >
            <th class="text-center">
              #
            </th>
            <th class="text-center">
              Material
            </th>
            <th class="text-center">
              Quantity
            </th>
          </tr>
        </thead>
        <tbody>
          @foreach($materials as $key)
          <tr id='addr0'>
            <td>
            1
            </td>
            <td>
            <input type="text" placeholder='Material' class="form-control" value="{{$key->id_material}}" readonly>
            </td>
            <td>
            <input type="text" placeholder='Quantity' class="form-control" value="{{$key->qty}}" readonly>
            </td>
          </tr>
          @endforeach
                    <tr id='addr1'></tr>
        </tbody>
      </table>
</div>
</div>

        
                          
        
        </div>

      @endsection  
@section('page-script')
<script>
       $(document).ready(function(){
      var i=1;
     $("#add_row").click(function(){
      $('#addr'+i).html("<td>"+ (i+1) +"</td><td><input  name='material["+i+"]' type='text' placeholder='Material'  class='form-control input-md'></td><td><input name='qty["+i+"]' type='text' placeholder='Quantity' class='form-control input-md'  /> </td>");

      $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
      i++; 
  });
     $("#delete_row").click(function(){
       if(i>1){
     $("#addr"+(i-1)).html('');
     i--;
     }
   });

});
</script>
@stop