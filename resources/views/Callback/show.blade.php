@extends('layouts.app')
@section('content')
      
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif

      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif

    <div class="row">
		 <div class="col-lg-12 margin-tb"></div>
		 <div class="form-group col-md-1">
			<div class="nav navbar-left">
			</div>
		</div>
	</div>

      <div class="row">
      	<div class="panel panel-default">
                <div class="panel-heading">Callback Detail</div>
                
                <div class="tab-container">
                  <ul class="nav nav-tabs nav-tabs-danger">
                    <li class="active"><a href="#user" data-toggle="tab">Pelanggan</a></li>
                    <li><a href="#account-receivable" data-toggle="tab">Survey</a></li>
                  </ul>
                  <div class="tab-content">
                    <div id="user" class="tab-pane active cont">
                      <div class="user-info-list panel panel-default">
                  <div class="panel-body">

                    <table class="no-border no-strip skills">
                      <tbody class="no-border-x no-border-y">
                        <tr>
                        	<td class="item">ID Pelanggan</td>
                          	<td class="item">: {{ $card->idpel}}</td>
                          	<td class="item">Nama</td>
                          	<td class="item">: {{ $card->nama}}</td>
                        </tr>
                        <tr>
                        	<td class="item">Nama Pemohon</td>
                          	<td class="item">: {{ $card->namapemohon }}</td>
                          	<td class="item">Alamat</td>
                          	<td class="item">: {{ $card->alamat}}</td>
                        </tr>
                         <tr>
                          <td class="item">No Telp / HP</td>
                            <td class="item">: {{ $card->notelp }} / {{ $card->nohp }}</td>
                            <td class="item">Alamat Pemohon</td>
                            <td class="item">: {{ $card->alamatpemohon}}</td>
                        </tr>
                     
                      </tbody>
                    </table>

                    <hr>
                    <table class="no-border no-strip skills">
                      <tbody class="no-border-x no-border-y">
                        </tr>
                        <tr>
                          <td class="item">No Register</span></td>
                          <td class="item">: {{ $card->noregister }}</td>
                          <td></td>
                        </tr>
                        <tr>
                        	<td class="item">Tanggal Mohon</span></td>
                          <td class="item">: {{ $card->tglmohon }}</td>
                          <td></td>
                        </tr>
                        <tr>
                        	<td class="item">Jenis Transaksi</td>
                          <td>: {{ $card->jenistransaksi }}</td>
					      <td></td>
                        </tr>
                        <tr>
                        	<td class="item">Tarif Lama</span></td>
                          <td class="item">: {{ $card->tariflama }} </td>
                          <td></td>
                        </tr>
                        <tr>
                        	<td class="item">Daya Lama</span></td>
                          <td class="item">: {{ $card->dayalama }}</td>
                          <td></td>
                        </tr>
                        <tr>
                        	<td class="item">Tarif </span></td>
                          <td class="item">: {{ $card->tarif }}</td>
                          <td></td>
                        </tr>
                        <tr>
                        	<td class="item">Daya</span></td>
                          <td class="item">: {{ $card->daya }}</td>
                          <td></td>
                        </tr>
                        <tr>
                        	<td class="item">Asal Mohon</span></td>
                          <td class="item">: {{ $card->asalmohon }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Jenis Bayar</span></td>
                          <td class="item">: {{ $card->jenisbayar }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Durasi Hari Kerja / Kalender</span></td>
                          <td class="item">: {{ $card->durasiharikerja }} / {{ $card->durasiharikalender }}</td>
                          <td></td>
                        </tr>

                        <tr>
                          <td class="item">Estimasi Durasi</span></td>
                          <td class="item">: {{ $card->estimasidurasi }}</td>
                          <td></td>
                        </tr>

                        <tr>
                        	<td class="item">Kriteria TMP</span></td>
                          <td class="item">: {{ $card->kriteriatmp }}</td>
                          <td></td>
                        </tr>

                        <tr>
                          <td class="item">Alasan Kriteria TMP</span></td>
                          <td class="item">: {{ $card->alasankriteriatmp }}</td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>


                      </tbody>
                    </table>

                  </div>
                </div>
               </div>
                    <div id="account-receivable" class="tab-pane cont">
                      <div class="user-info-list panel panel-default">
                     <div class="panel-body">

                    <table class="no-border no-strip skills">
                      <tbody class="no-border-x no-border-y">
                        </tr>
                        <tr>
                          <td class="item">Mohon maaf , apakah kami sedang berbicara dengan Bapak/Ibu DASRIL PILIANG</span></td>
                          <td class="item">: {{ $card->Survey->ans1 }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Apakah Bapak/Ibu melakukan sendiri proses permohonan Pasang baru/ Perubahan Daya ?</td>
                          <td>: {{ $card->Survey->ans2 }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Jika Bapak/Ibu tidak melakukan sendiri proses permohonan Pasang Baru, siapakah yang membantu dalam proses tersebut?</span></td>
                          <td>: {{ $card->Survey->ans3 }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Apakah Bapak/Ibu DASRIL PILIANG dikenakan biaya tambahan diluar biaya yang tertera pada nomor Registrasi Pasang Baru/ Perubahan Daya?</span></td>
                          <td>: {{ $card->Survey->ans4 }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Setelah Bapak/Ibu DASRIL PILIANG membayar biaya penyambungan, berapa lamakah waktu yang diperlukan hingga listrik menyala?</span></td>
                          <td>: {{ $card->Survey->ans5 }}</td>
                          <td></td>
                        </tr>
                        
                        <tr>
                          <td class="item">Kapan Bapak/Ibu DASRIL PILIANG memberikan biaya tambahan diluar ketentuan tersebut?</span></td>
                          <td class="item">: {{ $card->Survey->ans6 }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Apakah alasan Bapak/Ibu memberikan biaya tambahan diluar ketentuan tersebut?</span></td>
                          <td class="item">: {{ $card->Survey->ans7 }}</td>
                          <td></td>
                        </tr>
                        <tr>
                          <td class="item">Callback By</span></td>
                          <td class="item">: {{ $card->Operator->name }}</td>
                          <td></td>
                        </tr>

                      </tbody>
                    </table>

                      </div>
                    </div>
                  </div>
                   
              
      @endsection 