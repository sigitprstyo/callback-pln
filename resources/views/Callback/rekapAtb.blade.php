@extends('layouts.app')
@section('content')


    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif

     @if (\Session::has('errors'))
      <div class="alert alert-danger">
        <p>{{ \Session::get('errors') }}</p>
      </div><br />
      @endif

<div class="panel panel-default panel-border-color panel-border-color-danger">
                <div class="panel-heading panel-heading-divider">Rekapitulasi Adanya Tambahan Biaya</div>
                <div class="panel-body">
      <div class="table-responsive">
      <table id="table3" class="table table-striped">
              <thead>
                <tr>
                  <th>Nama Unit</th>
                  <th>Total Pelanggan</th>
                  <th>Ya, memberi biaya tambahan</th>
                  <th>Tidak mengeluarkan biaya tambahan lainnya</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($cards as $key)
                <tr>
                  <td>{{ $key->nama_rayon }}</td>
                  <td>{{ $key->total }}</td>
                  <td>{{ $key->ya }}</td>
                  <td>{{ $key->tidak }}</td>
                </tr>
                @endforeach
              </tbody>
    
    </table>
  </div>
  </div>
</div>
 @endsection   