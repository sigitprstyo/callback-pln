@extends('layouts.app')

@section('content')
      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif
      @if (\Session::has('success'))
      <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p> 
      </div><br />
      @endif

        <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default panel-border-color panel-border-color-danger">
                <div class="panel-heading panel-heading-divider">Upload Data Pelanggan</div>
                <div class="panel-body">
            <form method="post" action="{{url('/save')}}" enctype="multipart/form-data">
              {{csrf_field()}}
              
            

            <div class="row">
               <div class="col-lg-12 margin-tb"></div>
              <div class="form-group col-md-4">
                <label for="name">Data Pelanggan:</label>
                   <input type="file" class="form-control input-sm" name="file_pelanggan">
                   <a href="{{ asset('beagle/template.xlsx') }}">Download Template</a>
                </div>
              </div>

                <div class="form-group col-md-2">
                  <button type="submit" class="btn btn-primary btn-lg">Upload</button>
                  <a href=""></a>
              </div>

              </div>

                
            </form>

            </div>
          </div>
        </div>

@endsection  

@section('afterscript')
  <script src="{{ asset('beagle/lib/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/autoNumeric-1.9.18.js') }}"></script>
  <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jsgrid/1.5.3/jsgrid.min.js"></script> -->
  <script>
    $(".select2_demo_2").select2({
      width: '100%',
      height: '100%'
    });

    $("#id_lama_tempo_lain").hide();
    $("#id_pembayaran_lain").hide();
    $("#id_jaminan_lain").hide();
    $("#bank_garansi").hide();
    $("#id_syarat_penyerahan_lain").hide();
    $('#id_pembayaran').change(function(){
      // console.log("Change",$(this).val())
        if($('#id_pembayaran').val() == 'Lainnya') {
          $("#id_pembayaran_lain").show();
        }else{
          $("#id_pembayaran_lain").hide();
        }
    });
    $('#id_jaminan').change(function(){
      // console.log("Change",$(this).val())
        if($('#id_jaminan').val() == 'Lainnya') {
          $("#id_jaminan_lain").show();
          $("#bank_garansi").show();
        }else if($('#id_jaminan').val() == 'Tidak Ada'){
          $("#id_jaminan_lain").hide();
          $("#bank_garansi").hide();
        }else{
          $("#id_jaminan_lain").hide();
          $("#bank_garansi").show();
        }
    });
    $('#id_syarat_penyerahan').change(function(){
      // console.log("Change",$(this).val())
        if($('#id_syarat_penyerahan').val() == 'Lainnya') {
          $("#id_syarat_penyerahan_lain").show();
        }else{
          $("#id_syarat_penyerahan_lain").hide();
        }
    });
    $('#id_lama_tempo').change(function(){
      // console.log("Change",$(this).val())
        if($('#id_lama_tempo').val() == 'Lainnya') {
          $("#id_lama_tempo_lain").show();
        }else{
          $("#id_lama_tempo_lain").hide();
        }
    });

    $( window ).on( "load", function() {
        if($('#id_pembayaran').val() == 'Lainnya') {
          $("#id_pembayaran_lain").show();
        }else{
          $("#id_pembayaran_lain").hide();
        }
        if($('#id_jaminan').val() == 'Lainnya') {
          $("#id_jaminan_lain").show();
        }else{
          $("#id_jaminan_lain").hide();
        }
        if($('#id_syarat_penyerahan').val() == 'Lainnya') {
          $("#id_syarat_penyerahan_lain").show();
        }else{
          $("#id_syarat_penyerahan_lain").hide();
        }
        if($('#id_lama_tempo').val() == 'Lainnya') {
          $("#id_lama_tempo_lain").show();
        }else{
          $("#id_lama_tempo_lain").hide();
        }
    });

  $('.number').autoNumeric('init',{ 
    aSep: '.', 
    aDec: ',',
    mDec: 0,
    aForm: true,
    vMax: '999999999999',
    vMin: '-999999999999'
  });
  </script>
  <script type="text/javascript">
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    } 
  </script>
@endsection
