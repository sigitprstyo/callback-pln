@extends('layouts.app')

@section('afterhead')
    <link href="{{ asset('beagle/lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')
      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif
      @if (\Session::has('success'))
      <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p> 
      </div><br />
      @endif

        <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default panel-border-color panel-border-color-danger">
                <div class="panel-heading panel-heading-divider">Request Credit Approval Form</div>
                <div class="panel-body">
            <form method="post" action="{{action('Kredit\KreditController@update', $kredit->id)}}" enctype="multipart/form-data">
              {{csrf_field()}}
              
              <div class="row">
                <div class="form-group col-md-8">
                  <label>Customer:</label>
                  <select name="customer" class="select2_demo_2 form-control">
                    @foreach ($customer as $key)
                      @if ($key->customer_no==$kredit->Customer->customer_no)
                      <option value="{{$key->id}}" selected="true">{{$key->customer_no}} - {{$key->name}}</option>
                      @else
                      <option value="{{$key->id}}">{{$key->customer_no}} - {{$key->name}}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
              </div>

              <hr>

              <div class="row">
                <div class="col-lg-12 margin-tb"></div>

                <div class="form-group col-md-6">
                  <label>No Surat:</label>
                  <input type="text" name="no_surat" class="form-control input-sm" value="{{ $kredit->no_surat}}">
                </div>
                
              </div>

              <div class="row">
                <div class="col-lg-12 margin-tb"></div>
                <div class="col-sm-6">
                  <label>Jenis Produk:</label>
                  <select type="text" name="product[]" class="select2_demo_2 form-control" multiple="multiple">
                    @foreach ($product as $key)
                      @foreach ($kredit->Product as $no => $value)
                        @if ($key->material_no==$value->Detail->material_no)
                        <option value="{{$key->id}}" selected="true">{{$key->material_no}} - {{$key->material_name}}</option>
                        @else
                        <option value="{{$key->id}}">{{$key->material_no}} - {{$key->material_name}}</option>
                        @endif
                      @endforeach
                    @endforeach
                  </select>
                </div>

                <div class="form-group col-sm-2">
                  <label>Perkiraan Volume:</label>
                  <input type="text" name="volume" class="form-control input-sm number" value="{{ $kredit->volume }}" onkeypress="javascript:return isNumber(event)">
                </div>

                <div class="form-group col-sm-2">
                  <label>UoM:</label>
                   <select type="text" name="satuan" class="form-control input-sm">
                      <option value="KL" {{ ($kredit->satuan=='KL' ? 'selected':'')}}>KL</option>
                      <option value="MT" {{ ($kredit->satuan=='MT' ? 'selected':'')}}>MT</option>
                      <option value="LtrObs" {{ ($kredit->satuan=='LtrObs' ? 'selected':'')}}>LtrObs</option>
                      <option value="Ltr 15" {{ ($kredit->satuan=='Ltr 15' ? 'selected':'')}}>Ltr 15</option>
                      <option value="BB6" {{ ($kredit->satuan=='BB6' ? 'selected':'')}}>BB6</option>
                  </select>
                </div>

                <div class="form-group col-sm-2">
                  <label>Periode Volume</label>
                   <select type="text" name="periode_volume" class="form-control input-sm">
                    <option value="Harian" {{ ($kredit->periode_volume=='Harian' ? 'selected':'')}}>Harian</option>
                    <option value="Mingguan" {{ ($kredit->periode_volume=='Mingguan' ? 'selected':'')}}>Mingguan</option>
                    <option value="Bulanan" {{ ($kredit->periode_volume=='Bulanan' ? 'selected':'')}}>Bulanan</option>
                    <option value="Tahunan" {{ ($kredit->periode_volume=='Tahunan' ? 'selected':'')}}>Tahunan</option>
                    <option value="Triwulan" {{ ($kredit->periode_volume=='Triwulan' ? 'selected':'')}}>Triwulan</option>
                    <option value="Semester" {{ ($kredit->periode_volume=='Semester' ? 'selected':'')}}>Semester</option>
                  </select>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12 margin-tb"></div>

                <div class="form-group col-md-2">
                  <label>Periode Penagihan:</label>
                  <select type="text" name="periode_penyerahan" class="form-control input-sm">
                    @foreach ($periode_penagihan as $key)
                      <option value="{{$key}}" {{($kredit->periode_penyerahan==$key ? 'selected':'')}}>{{$key}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group col-md-5">
                  <label>Jatuh Tempo:</label>
                  <select type="text" name="lama_tempo" class="form-control select2_demo_2 input-sm"  id="id_lama_tempo">
                    @foreach ($jatuh_tempo as $key)
                      <option value="{{$key}}" {{($kredit->lama_tempo==$key ? 'selected':'')}}>{{$key}}</option>
                    @endforeach
                  </select>

                </div>

                <div class="form-group col-md-5" id="id_lama_tempo_lain">
                  <label>Jatuh Tempo Lain:</label>
                  <input type="text" name="lama_tempo_lain" class="form-control input-sm" value="{{ $kredit->lama_tempo_lain }}">
                </div>
              </div>

                <div class="row">
                <div class="col-lg-12 margin-tb"></div>
                <div class="form-group col-md-3">
                  <label>Perkiraan Nilai Transaksi:</label>
                  <input type="text" name="nilai_transaksi" class="form-control input-sm number" value="{{$kredit->nilai_transaksi }}" onkeypress="javascript:return isNumber(event)">
                </div>

                <div class="form-group col-md-3">
                  <label>Credit Limit:</label>
                  <input type="text" name="credit_limit" class="form-control input-sm number" value="{{ $kredit->credit_limit }}" onkeypress="javascript:return isNumber(event)">
                </div>
                
              </div>

                <hr>

              <div class="row">
                <div class="col-lg-12 margin-tb"></div>
                <div class="form-group col-md-6">
                  <label>Mekanisme Pembayaran:</label>
                  <select type="text" name="pembayaran" class="select2_demo_2 form-control" id="id_pembayaran">
                    @foreach ($bank as $key)
                      <option value="{{$key}}" {{($kredit->pembayaran==$key ? 'selected':'')}}>{{$key}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group col-md-6">
                  <label>Bentuk Jaminan yang Diusulkan:</label>
                  <select type="text" name="jaminan" class="form-control input-sm" id="id_jaminan">
                    @foreach ($jaminan as $key)
                      <option value="{{$key}}" {{($kredit->jaminan==$key ? 'selected':'')}}>{{$key}}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="row">
                <div class="form-group col-md-6" id="id_pembayaran_lain">
                  <label>Mekanisme Pembayaran Lainnya:</label>
                  <input type="text" name="pembayaran_lain" class="form-control input-sm" value="{{$kredit->pembayaran_lain}}">
                </div>

                <div class="form-group col-md-6" id="id_jaminan_lain">
                  <label>Bentuk Jaminan Lainnya:</label>
                  <input type="text" name="jaminan_lain" class="form-control input-sm" value="{{$kredit->jaminan_lain}}">
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12 margin-tb"></div>
                <div class="form-group col-md-3">
                  <label>Sanksi Keterlambatan:</label>
                  <select type="text" name="flag_denda" class="form-control input-sm">
                    <option value="0" {{ ($kredit->flag_denda=='0' ? 'selected':'')}}>Tidak</option>
                    <option value="1" {{ ($kredit->flag_denda=='1' ? 'selected':'')}}>Ya</option>
                  </select>
                </div>

                <div class="form-group col-md-3">
                  <label>Syarat Penyerahan:</label>
                  <select type="text" name="syarat_penyerahan" class="form-control input-sm" id="id_syarat_penyerahan">
                    @foreach ($syarat_penyerahan as $key)
                      <option value="{{$key}}" {{($kredit->syarat_penyerahan==$key ? 'selected':'')}}>{{$key}}</option>
                    @endforeach
                  </select>
                </div>

                <div class="form-group col-md-6" id="id_syarat_penyerahan_lain">
                  <label>Syarat Penyerahan Lainnya:</label>
                  <input type="text" name="syarat_penyerahan_lain" class="form-control input-sm" value="{{$kredit->syarat_penyerahan_lain}}">
                </div>
              </div>

                <hr>

               <div class="row" id="data_1">
                <div class="col-lg-12 margin-tb"></div>
                <div class="form-group col-md-3 date">
                    <label>Jangka Waktu dari</label>
                      <div class="col-md-12 input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <input type="text" class="form-control input-sm" name="tempo_start" value="{{date('Y-m-d', strtotime($kredit->tempo_start))}}">
                        </div>
                    </div>

                <div class="form-group col-md-3 date">
                    <label>Jangka Waktu ke</label>
                      <div class="col-md-12 input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                          <input type="text" class="form-control input-sm" name="tempo_end" value="{{date('Y-m-d', strtotime($kredit->tempo_end))}}">
                        </div>
                    </div>

                  </div>

                  <hr>

            <div class="row">
               <div class="col-lg-12 margin-tb"></div>
              <div class="form-group col-md-6">
                <label for="name">Memo Pengantar:</label>
                   <input type="file" class="form-control input-sm" name="memo_pengantar" >
                </div>
              </div>

              <div class="row">
               <div class="col-lg-12 margin-tb"></div>
              <div class="form-group col-md-3">
                <label for="name">Dokumen Pendukung:</label><br>
                <label for="name">1. Laporan Keuangan Audited</label>  
                   <input type="file" class="form-control input-sm" name="doc_lka" value="{{Request::old('doc_lka')}}">
                </div>

              <div class="form-group col-md-3">
                <label for="name">&nbsp;</label><br>
                <label for="name">2. Credit Approval Sebelumnya</label>  
                   <input type="file" class="form-control input-sm" name="doc_cas" value="{{Request::old('doc_cas')}}">
                </div>

              <div class="form-group col-md-3">
                <label for="name">&nbsp;</label><br>
                <label for="name">3. Bank Garansi</label>  
                   <input type="file" class="form-control input-sm" name="doc_bg" value="{{Request::old('doc_bg')}}">
                </div>

              <div class="form-group col-md-3">
                <label for="name">&nbsp;</label><br>
                <label for="name">4. Pocket Margin dan lainnya</label>  
                   <input type="file" class="form-control input-sm" name="doc_pml" value="{{Request::old('doc_pml')}}">
                </div>

              <div class="row">
                <div class="col-lg-12 margin-tb"></div>
                <div class="form-group col-md-6">
                  <button type="submit" class="btn btn-success btn-lg">Update</button>
                </div>
              </div>
            </form>

            </div>
          </div>
        </div>

@endsection  

@section('afterscript')
  <script src="{{ asset('beagle/lib/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/autoNumeric-1.9.18.js') }}"></script>
  <script>
    $(".select2_demo_2").select2({
      width: '100%',
      height: '100%'
    });



    $("#id_pembayaran_lain").hide();
    $("#id_lama_tempo_lain").hide();
    $("#id_jaminan_lain").hide();
    $("#id_syarat_penyerahan_lain").hide();
    $('#id_pembayaran').change(function(){
      // console.log("Change",$(this).val())
        if($('#id_pembayaran').val() == 'Lainnya') {
          $("#id_pembayaran_lain").show();
        }else{
          $("#id_pembayaran_lain").hide();
        }
    });
    $('#id_jaminan').change(function(){
      // console.log("Change",$(this).val())
        if($('#id_jaminan').val() == 'Lainnya') {
          $("#id_jaminan_lain").show();
        }else{
          $("#id_jaminan_lain").hide();
        }
    });
    $('#id_syarat_penyerahan').change(function(){
      // console.log("Change",$(this).val())
        if($('#id_syarat_penyerahan').val() == 'Lainnya') {
          $("#id_syarat_penyerahan_lain").show();
        }else{
          $("#id_syarat_penyerahan_lain").hide();
        }
    });
    $('#id_lama_tempo').change(function(){
      // console.log("Change",$(this).val())
        if($('#id_lama_tempo').val() == 'Lainnya') {
          $("#id_lama_tempo_lain").show();
        }else{
          $("#id_lama_tempo_lain").hide();
        }
    });

    $( window ).on( "load", function() {
        if($('#id_pembayaran').val() == 'Lainnya') {
          $("#id_pembayaran_lain").show();
        }else{
          $("#id_pembayaran_lain").hide();
        }
        if($('#id_jaminan').val() == 'Lainnya') {
          $("#id_jaminan_lain").show();
        }else{
          $("#id_jaminan_lain").hide();
        }
        if($('#id_syarat_penyerahan').val() == 'Lainnya') {
          $("#id_syarat_penyerahan_lain").show();
        }else{
          $("#id_syarat_penyerahan_lain").hide();
        }
        if($('#id_lama_tempo').val() == 'Lainnya') {
          $("#id_lama_tempo_lain").show();
        }else{
          $("#id_lama_tempo_lain").hide();
        }
    });

  $('.number').autoNumeric('init',{ 
    aSep: '.', 
    aDec: ',',
    mDec: 0,
    aForm: true,
    vMax: '999999999999',
    vMin: '-999999999999'
  });
  </script>
  <script type="text/javascript">
    function isNumber(evt) {
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;

        return true;
    } 
  </script>
@stop
