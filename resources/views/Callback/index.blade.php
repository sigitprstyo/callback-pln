@extends('layouts.app')
@section('content')

    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif

     @if (\Session::has('errors'))
      <div class="alert alert-danger">
        <p>{{ \Session::get('errors') }}</p>
      </div><br />
      @endif

<div class="panel panel-default panel-border-color panel-border-color-danger">
                <div class="panel-heading panel-heading-divider">List Belum Callback
                <button class="btn btn-space btn-warning btn-sm pull-right"  data-toggle="modal" data-target="#filterModal"><i class="icon mdi mdi-filter-list"></i> Filter</button>
                </div>
                <div class="panel-body">
      <div class="table-responsive">
      <table id="table3" class="table table-striped">
      <thead>
                <tr>
                  <th>No Register</th>
                  <th>ID Pelanggan</th>
                  <th>No Telepon/No HP</th>
                  <th>Nama</th>    
                  <th>Jenis Transaksi</th>
                  <th>Tarif / Daya</th>
                  <th>Asal Mohon</th>
                  <th>Durasi Kerja / Kalender</th>
                  <th></th>
                  @if (Auth::user()->isRole('Administrator')) <th></th> @endif

                </tr>
              </thead>
              <tbody>
                @foreach ($cards as $key)
                <tr>
                  <td>{{ $key->noregister }}</td>
                  <td>{{ $key->idpel }}</td>
                  <td>{{ $key->notelp }}<br>{{ $key->nohp }}</td>
                  <td>{{ $key->nama }}</td>
                  <td>{{ $key->jenistransaksi }}</td>
                  <td>{{ $key->tarif }}/{{ $key->daya }}</td>
                  <td>{{ $key->asalmohon }}</td>
                  <td>{{ $key->durasiharikerja }}/{{ $key->durasiharikalender }}</td>
                  <td> 
                      <a href="{{action('Master\PelangganController@action', $key->id )}}" title="Survey"><span class="btn btn-sm btn-warning"><i class="mdi mdi-file-plus"></i></span></a>
                    </td>
                  @if (Auth::user()->isRole('Administrator')) 
                  <td> 
                      <a href="{{action('Master\PelangganController@delete', $key->id )}}" onclick="return confirm('Yakin akan hapus data?')" title="Delete"><span class="btn btn-sm btn-danger"><i class="mdi mdi-close"></i></span></a>
                    </td>
                  @endif
                    
                </tr>
                @endforeach
                
              </tbody>
    
    </table>
    {{ $cards->links() }}
  </div>
  </div>
</div>

<!-- Filter Modal -->
<div id="filterModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Filter </h4>
      </div>
      <div class="modal-body">
      <form method="post" name="filterPost" action="{{url($url)}}">
      {{csrf_field()}}
      <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-11">
            <label for="nama_penyulang">Bulan:</label>
            <select type="text" name="month" class="form-control">
              <option value="{{ date('m') }}">{{ date('F') }}</option>
              <option value="01">Januari</option>
              <option value="02">Februari</option>
              <option value="03">Maret</option>
              <option value="04">April</option>
              <option value="05">Mei</option>
              <option value="06">Juni</option>
              <option value="07">Juli</option>
              <option value="08">Agustus</option>
              <option value="09">September</option>
              <option value="10">Oktober</option>
              <option value="11">November</option>
              <option value="12">Desember</option>
            </select>
          </div>
        </div>

      <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-11">
            <label for="nama_penyulang">Tahun:</label>
            <select type="text" name="year" class="form-control">
              <option value="{{ date('Y') }}">{{ date('Y') }}</option>
              <option value="{{ date('Y', strtotime('- 1 year')) }} ">{{ date('Y', strtotime('- 1 year')) }}</option>
              <option value="{{ date('Y', strtotime('- 2 year')) }} ">{{ date('Y', strtotime('- 2 year')) }}</option>
              <option value="{{ date('Y', strtotime('- 3 year')) }} ">{{ date('Y', strtotime('- 3 year')) }}</option>
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-lg btn-success" value="Submit"></button>
      </div>
      </form>
    </div>
  </div>
</div>
 @endsection   