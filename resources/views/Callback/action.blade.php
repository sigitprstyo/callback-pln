@extends('layouts.app')

@section('content')
      

        <div class="row">
            <div class="col-sm-12">
              <div class="panel panel-default panel-border-color panel-border-color-danger">
                <div class="panel-heading panel-heading-divider">Survey Callback</div>
                <div class="panel-body">
                  <div class="main-content container-fluid">
                    <div id="accordion1" class="panel-group accordion">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1" href="#collapseOne"><i class="icon mdi mdi-chevron-down"></i> Data Pelanggan</a></h4>
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body">
                          <h4>Data Nomor</h4>
                          <hr>
                          <div class="row">
                            <div class="form-group col-sm-6">
                              <strong>No. Telepon:</strong>
                              <p>{{ $pelanggan->notelp}}</p>
                            </div>

                            <div class="form-group col-sm-6">
                              <strong>No. HP:</strong>
                              <p>{{ $pelanggan->nohp}}</p>
                            </div>
                          </div>

                          <div class="row">
                            <div class="form-group col-sm-6">
                              <strong>No. Registrasi:</strong>
                              <p>{{ $pelanggan->noregister}}</p>
                            </div>

                          </div>

                          <h4>Data Pelanggan</h4>
                          <hr>

                          <div class="row">
                            <div class="form-group col-sm-6">
                              <strong>Kode Unit:</strong>
                              <p>{{ $pelanggan->unitup}}</p>
                            </div>

                            <div class="form-group col-sm-6">
                              <strong>Nama Unit:</strong>
                              <p>{{ $pelanggan->Unit->nama_rayon }}</p>
                            </div>
                          </div>

                          <div class="row">
                          <div class="form-group col-sm-6">
                              <strong>ID Pelanggan:</strong>
                              <p>{{ $pelanggan->idpel}}</p>
                            </div>

                            <div class="form-group col-sm-6">
                              <strong>Nama Pelanggan:</strong>
                              <p>{{ $pelanggan->nama }}</p>
                            </div>
                          </div>

                            <div class="row">
                            <div class="form-group col-sm-6">
                              <strong>Alamat:</strong>
                              <p>{{ $pelanggan->alamat}}</p>
                            </div>

                            <div class="form-group col-sm-6">
                              <strong>Asal Mohon:</strong>
                              <p>{{ $pelanggan->asalmohon }}</p>
                            </div>
                          </div>

                          <div class="row">
                            <div class="form-group col-sm-6">
                              <strong>Taif Lama:</strong>
                              <p>{{ $pelanggan->tariflama}}</p>
                            </div>

                            <div class="form-group col-sm-6">
                              <strong>Daya Lama:</strong>
                              <p>{{ $pelanggan->dayalama }}</p>
                            </div>
                          </div>

                          <div class="row">
                            <div class="form-group col-sm-6">
                              <strong>Tarif:</strong>
                              <p>{{ $pelanggan->tarif}}</p>
                            </div>

                            <div class="form-group col-sm-6">
                              <strong>Daya:</strong>
                              <p>{{ $pelanggan->daya }}</p>
                            </div>
                          </div>

                          <div class="row">
                            <div class="form-group col-sm-6">
                              <strong>Total Biaya:</strong>
                              <p>Rp. {{ number_format($pelanggan->rpbp) }}</p>
                            </div>
                          </div>
                        </div>

                            <h4>Tanggal Transaksi</h4>
                            <hr>

                            <div class="row">

                            <div class="form-group col-sm-6">
                              <strong>Tanggal Bayar:</strong>
                              <p>{{ date('d/m/Y', strtotime($pelanggan->tglbayar)) }}</p>
                            </div>


                            <div class="form-group col-sm-6">
                              <strong>Tanggal PK:</strong>
                              <p>{{ date('d/m/Y', strtotime($pelanggan->tglpk)) }}</p>
                            </div>
                          </div>

                          </div>

                          </div>
                         </div>

          <div class="row wizard-row">
            <div class="col-md-12 fuelux">
              <div class="block-wizard panel panel-default">
                <div id="wizard1" class="wizard wizard-ux">
                  <ul class="steps">
                    <li data-step="1" class="active">Petugas<span class="chevron"></span></li>
                    <li data-step="2">Survey Layanan PB/PD<span class="chevron"></span></li>
                    <li data-step="3">Closing<span class="chevron"></span></li>
                  </ul>
                  <div class="actions">
                    <button type="button" class="btn btn-xs btn-prev btn-default"><i class="icon mdi mdi-chevron-left"></i>Prev</button>
                    <button type="button" data-last="Finish" class="btn btn-xs btn-next btn-default">Next<i class="icon mdi mdi-chevron-right"></i></button>
                  </div>
                  <form method="post" action="{{ action('Master\PelangganController@saveSurvey', $pelanggan->id) }}">
                    {{csrf_field()}}
                  <div class="step-content">
                    <div data-step="1" class="step-pane active">
                      <div class="row">
                                  <div class="form-group">
                                    <label class="col-sm-3 control-label">Nama Petugas Call Back</label>
                                    <div class="col-sm-6">
                                      <input type="text" placeholder="Nama Petugas" name="nama_petugas" class="form-control input-sm" value="{{ Auth::user()->name }}">
                                    </div>
                                  </div>
                                </div>
                                  <hr>
                                  <div class="row">
                                  <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                      <a class="btn btn-default btn-space">Cancel</a>
                                      <a data-wizard="#wizard1" class="btn btn-primary btn-space wizard-next">Next Step</a>
                                    </div>
                                  </div>
                                </div>
                                
                              </div>
                    <div data-step="2" class="step-pane">
                      
                          <div class="row">
                            <div class="col-md-12">
                              <div class="row">
                                <div class="col-md-12">
                                   <label class="col-sm-12">Selamat Siang Bapak/ Ibu {{$pelanggan->nama}} yang terhormat, saya dari CALL BACK CENTER SUMUT bermaksud menanyakan beberapa hal terkait pelayanan dan pelaksanaan PASANG BARU/ PERUBAHAN DAYA di tempat Bapak/ Ibu, mohon kesediaan waktunya.</label>
                                 </div>
                               </div>
                             </div>
                           </div>
                           <hr>

                          <div class="row" id="srv1">
                            <div class="col-md-12">
                              <div class="row">
                                <div class="col-md-12">
                                <label class="col-sm-12">Mohon maaf , apakah kami sedang berbicara dengan Bapak/Ibu {{ $pelanggan->nama }}</label>
                              </div>
                            </div>
                              <div class="row">
                                <div class="col-md-11 pull-right">
                                <div class="be-radio">
                                  <input type="radio" name="ans1" id="ans11" value="Ya">
                                  <label for="ans11">Ya</label>
                                </div>
                                <div class="be-radio">
                                  <input type="radio" name="ans1" id="ans12" value="Tidak">
                                  <label for="ans12">Tidak</label>
                                </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        <hr id="hrtelp">
                        <div class="row" id="srvtelp">
                            <div class="col-md-12">
                              <div class="row">
                                <div class="col-md-12">
                                <label class="col-sm-12">No. Telpon yang dapat dihubungi</label>
                              </div>
                            </div>
                              <div class="row">
                                <div class="col-md-11 pull-right">
                                <div class="form-group">
                                  <input type="text" name="notelp" class="form-control">
                                </div>
                                
                                </div>
                              </div>
                            </div>
                        </div>
                        <hr id="hr1">
                        <div class="row" id="srv2">
                            <div class="col-md-12">
                              <div class="row">
                                <div class="col-md-12">
                                <label class="col-sm-12">Apakah Bapak/Ibu {{$pelanggan->nama}} melakukan sendiri proses permohonan Pasang baru/ Perubahan Daya ?</label>
                              </div>
                            </div>
                              <div class="row">
                                <div class="col-md-11 pull-right">
                                <div class="be-radio">
                                  <input type="radio" name="ans2" id="ans21" value="Tidak">
                                  <label for="ans21">Tidak</label>
                                </div>
                                <div class="be-radio">
                                  <input type="radio" name="ans2" id="ans22" value="Ya, melalui CC123,web PLN atau Loket PLN">
                                  <label for="ans22">Ya, melalui CC123,web PLN atau Loket PLN</label>
                                </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        <hr id="hr2">
                         <div class="row" id="srv3">
                            <div class="col-md-12">
                              <div class="row">
                                <div class="col-md-12">
                                <label class="col-sm-12">Jika Bapak/Ibu {{ $pelanggan->nama }} tidak melakukan sendiri proses permohonan Pasang Baru, siapakah yang membantu dalam proses tersebut?</label>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-11 pull-right">
                              <div class="be-radio">
                                        <input type="radio" name="ans3" id="ans31" value="Petugas PLN yang berada disekitar kantor PLN">
                                        <label for="ans31">Petugas PLN yang berada disekitar kantor PLN</label>
                                      </div>
                                      <div class="be-radio">
                                        <input type="radio" name="ans3" id="ans32" value="Petugas lapangan PLN (Baca Meter, Pemutusan/Penyambungan, Layanan Teknik)">
                                        <label for="ans32">Petugas lapangan PLN (Baca Meter, Pemutusan/Penyambungan, Layanan Teknik)</label>
                                      </div>
                                      <div class="be-radio">
                                        <input type="radio" name="ans3" id="ans33" value="Perusahaan biro jasa / instalatir dan sejenisnya">
                                        <label for="ans33">Perusahaan biro jasa / instalatir dan sejenisnya</label>
                                      </div>
                                      <div class="be-radio">
                                        <input type="radio" name="ans3" id="ans34" value="Selain ketiga poin diatas">
                                        <label for="ans34">Selain ketiga poin diatas</label>
                                      </div>
                              </div>
                            </div>
                            </div>
                          </div>
                          <hr id="hr3">
                                <div class="row"  id="srv4">
                                    <div class="col-md-12">
                                      <div class="row">
                                        <div class="col-md-12">
                                        <label class="col-sm-12">Setelah Bapak/Ibu {{$pelanggan->nama}} membayar biaya penyambungan, berapa lamakah waktu yang diperlukan hingga listrik menyala?</label>
                                      </div>
                                    </div>
                                      <div class="row">
                                        <div class="col-md-11 pull-right">
                                        <div class="be-radio">
                                          <input type="radio" name="ans4" id="ans41" value="1 hari s/d 3 hari kerja">
                                          <label for="ans41">1 hari s/d 3 hari kerja</label>
                                        </div>
                                        <div class="be-radio">
                                          <input type="radio" name="ans4" id="ans42" value="4 hari s/d 10 hari kerja">
                                          <label for="ans42">4 hari s/d 10 hari kerja</label>
                                        </div>
                                        <div class="be-radio">
                                          <input type="radio" name="ans4" id="ans43" value="Lebih dari 10 hari kerja">
                                          <label for="ans43">Lebih dari 10 hari kerja</label>
                                        </div>
                                        <div class="be-radio">
                                          <input type="radio" name="ans4" id="ans44" value="Belum menyala">
                                          <label for="ans44">Belum menyala</label>
                                        </div>

                                        </div>
                                      </div>


                                    </div>
                                </div>
                            <hr id="hr4">
                                <div class="row" id="srv5">
                                    <div class="col-md-12">
                                      <div class="row">
                                        <div class="col-md-12">
                                        <label class="col-sm-12">Apakah Bapak/Ibu {{$pelanggan->nama}} dikenakan biaya tambahan diluar biaya yang tertera pada nomor Registrasi Pasang Baru/ Perubahan Daya?</label>
                                      </div>
                                    </div>
                                      <div class="row">
                                        <div class="col-md-11 pull-right">
                                        <div class="be-radio">
                                          <input type="radio" name="ans5" id="ans51" value="Ya, memberi biaya tambahan">
                                          <label for="ans51">Ya, memberi biaya tambahan</label>
                                        </div>
                                        <div class="be-radio">
                                          <input type="radio" name="ans5" id="ans52" value="Tidak mengeluarkan biaya tambahan lainnya">
                                          <label for="ans52">Tidak mengeluarkan biaya tambahan lainnya</label>
                                        </div>

                                        </div>
                                      </div>


                                    </div>
                                </div>
                                
                                <hr id="hr5">
                                <div class="row" id="srv6">
                                    <div class="col-md-12">
                                      <div class="row">
                                        <div class="col-md-12">
                                        <label class="col-sm-12">Kapan Bapak/Ibu {{$pelanggan->nama}} memberikan biaya tambahan diluar ketentuan tersebut?</label>
                                      </div>
                                    </div>
                                      <div class="row">
                                        <div class="col-md-11 pull-right">
                                        <div class="be-radio">
                                          <input type="radio" name="ans6" id="ans61" value="Awal proses permohonan">
                                          <label for="ans61">Awal proses permohonan</label>
                                        </div>
                                        <div class="be-radio">
                                          <input type="radio" name="ans6" id="ans62" value="Saat proses permohonan">
                                          <label for="ans62">Saat proses permohonan</label>
                                        </div>
                                        <div class="be-radio">
                                          <input type="radio" name="ans6" id="ans63" value="Setelah proses permohonan">
                                          <label for="ans63">Setelah proses permohonan</label>
                                        </div>

                                        </div>
                                      </div>
                                      <hr id="hr6">
                                      <div class="row" id="srv7">
                                          <div class="col-md-12">
                                            <div class="row">
                                              <div class="col-md-12">
                                              <label class="col-sm-12">Apakah alasan Bapak/Ibu memberikan biaya tambahan diluar ketentuan tersebut?</label>
                                            </div>
                                          </div>
                                            <div class="row">
                                              <div class="col-md-11 pull-right">
                                              <div class="be-radio">
                                                <input type="radio" name="ans7" id="ans71" value="Agar proses Pasang Baru/ Perubahan Daya dapat dipercepat pengerjaannya">
                                                <label for="ans71">Agar proses Pasang Baru/ Perubahan Daya dapat dipercepat pengerjaannya</label>
                                              </div>
                                              <div class="be-radio">
                                                <input type="radio" name="ans7" id="ans72" value="Untuk biaya Jaminan Instalasi dan SLO ( Sertifikat Laik Operasi)">
                                                <label for="ans72">Untuk biaya Jaminan Instalasi dan SLO ( Sertifikat Laik Operasi)</label>
                                              </div>
                                              <div class="be-radio">
                                                <input type="radio" name="ans7" id="ans73" value="Sebagai ucapan teriman kasih kepada petugas">
                                                <label for="ans73">Sebagai ucapan teriman kasih kepada petugas</label>
                                              </div>

                                              </div>
                                            </div>
                                      <hr id="hr7">
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-12">
                            <a data-wizard="#wizard1" class="btn btn-default btn-space wizard-previous">Previous</a>
                            <a data-wizard="#wizard1" class="btn btn-primary btn-space wizard-next">Next Step</a>
                          </div>
                        </div>
                      
                    </div>
                    <div data-step="3" class="step-pane">
                      <div class="row">
                        <div class="form-group no-padding">
                          <div class="col-sm-7">
                            <h3 class="wizard-title">Closing</h3>
                          </div>
                        </div>
                      </div>
                        <hr>
                        <div class="row">
                        <div class="form-group">
                          <div class="row">
                                    <div class="col-md-12">
                                      <div class="row">
                                        <div class="col-md-12">
                                        <label class="col-sm-12">Terima kasih Bapak/ Ibu {{$pelanggan->nama}} telah meluangkan waktu untuk kami.Jawaban ini sangat berarti bagi kami untuk meningkatkan mutu pelayanan PLN. Selamat Pagi/ Siang/ Malam.</label>
                                      </div>
                                    </div>
                                    <hr>
                                      <div class="row" id="srv7">
                                          <div class="col-md-12">
                                            <div class="row">
                                              <div class="col-md-12">
                                              <label class="col-sm-12">Kritik & Saran dari pelanggan {{ $pelanggan->nama }}</label>
                                            </div>
                                          </div>
                                            <div class="row">
                                              <div class="col-md-11 pull-right">
                                              <div class="be-radio">
                                                <input type="text" name="kritiksaran" class="form-control input-sm">
                                              </div>

                                              </div>
                                            </div>
                                      <hr id="hr7">
                                    </div>
                                </div>


                                    </div>
                                </div>
                        </div>
                      </div>
                        
                        <div class="form-group">
                          <div class="col-sm-12">
                            <a data-wizard="#wizard1" class="btn btn-default btn-space wizard-previous">Previous</a>
                            <input type="submit" name="submit" class="btn btn-success btn-space" value="Submit">
                          </div>
                        </div>

                        </form>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

            </div>
          </div>
        </div>

@endsection  

@section('afterscript')
  <script src="{{ asset('beagle/lib/select2/js/select2.full.min.js') }}"></script>
  <script src="{{ asset('js/autoNumeric-1.9.18.js') }}"></script>

    <script src="{{ asset('beagle/lib/fuelux/js/wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/lib/select2/js/select2.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/lib/bootstrap-slider/js/bootstrap-slider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/js/app-form-wizard.js') }}" type="text/javascript"></script>

<script type="text/javascript">

    $("#srv1").show();
    $("#srv2").hide();
    $("#srv3").hide();
    $("#srv4").hide();
    $("#srv5").hide();
    $("#srv6").hide();
    $("#srv7").hide();

    $("#srvtelp").hide();

    $("#hr1").show();
    $("#hr2").hide();
    $("#hr3").hide();
    $("#hr4").hide();
    $("#hr5").hide();
    $("#hr6").hide();
    $("#hr7").hide();

    $("#hrtelp").hide();
    
    $('input[type=radio][name=ans1]').change(function() {
      var radioValue = $("input[name='ans1']:checked").val();
        if(radioValue==='Ya') {
          $("#srv2").show();
          $("#hr2").show();

          $("#srvtelp").hide();
          $("#hrtelp").hide();  
        }else if(radioValue === 'Tidak') {
          $("#srv2").hide();
          $("#hr2").hide();

          $("#srvtelp").show();
          $("#hrtelp").show();
        }
    });

    $('input[type=radio][name=ans2]').change(function() {
      var radioValue = $("input[name='ans2']:checked").val();
        if(radioValue==='Ya, melalui CC123,web PLN atau Loket PLN') {
          $("#srv3").hide();
          $("#srv4").show();

          $("#hr3").hide();
          $("#hr4").show();
          
        }else if(radioValue === 'Tidak') {
          $("#srv3").show();
          $("#srv4").hide();

          $("#hr3").show();
          $("#hr4").hide();
        }
    });

    $('input[type=radio][name=ans3]').change(function() {
      $("#srv4").show();
      $("#hr4").show();
    });
    $('input[type=radio][name=ans4]').change(function() {
      $("#srv5").show();
      $("#hr5").show();
    });
    $('input[type=radio][name=ans5]').change(function() {
      var radioValue = $("input[name='ans5']:checked").val();
        if(radioValue==='Ya, memberi biaya tambahan') {
          $("#srv6").show();
          $("#srv7").show();

          $("#hr6").show();
          $("#hr7").show();
        }else if(radioValue === 'Tidak mengeluarkan biaya tambahan lainnya') {
          $("#srv6").hide();
          $("#srv7").hide();

          $("#hr6").hide();
          $("#hr7").hide();
        }
    });

   //Fuel UX
    $('.wizard-ux').wizard();

    $('.wizard-ux').on('changed.fu.wizard',function(){
      //Bootstrap Slider
      $('.bslider').slider();
    });
    
    $(".wizard-next").click(function(e){
      var id = $(this).data("wizard");
      $(id).wizard('next');
      e.preventDefault();
    });
    
    $(".wizard-previous").click(function(e){
      var id = $(this).data("wizard");
      $(id).wizard('previous');
      e.preventDefault();
    });

</script>
@endsection
