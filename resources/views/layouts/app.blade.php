<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title','Call Back - PLN WILSU')</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('beagle/lib/perfect-scrollbar/css/perfect-scrollbar.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle/lib/material-design-icons/css/material-design-iconic-font.min.css') }}"/><!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="{{ asset('beagle/css/style.css') }}" type="text/css"/>
    <!-- <link rel="stylesheet" type="text/css" href="{{ asset('beagle/lib/summernote/summernote.css') }}"/> -->
    <link rel="stylesheet" href="{{ asset('beagle/lib/datapicker/datepicker3.css') }}" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('beagle/lib/dataTables2/datatables.min.css') }}"/>
    
    @yield('afterhead')
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
      <nav class="navbar navbar-default navbar-fixed-top be-top-header"  style="background-color: #2E93B1;">
        <div class="container-fluid">
          
          <div class="be-right-navbar">
            <ul class="nav navbar-nav navbar-right be-user-nav">
              <li class="dropdown"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="dropdown-toggle"><img src="{{ asset('beagle/img/user.png') }}" style="max-height: 30px;" alt="Avatar"></a>
                <ul role="menu" class="dropdown-menu">
                  @guest
                  <li><a href="{{ route('register') }}"><span class="icon mdi mdi-face"></span>Sign Up</a></li>
                  @else
                  <li>
                    <div class="user-info"  style="background-color: #2E93B1;">
                      <div class="user-name">{{ Auth::user()->name }}</div>
                    </div>
                  </li>
                  <li><a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><span class="icon mdi mdi-power"></span> Logout</a></li>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
                  @endguest
                </ul>
              </li>
            </ul>
            <div class="page-title"><span style="color: white;font-weight: bold;">CALL BACK CENTER PLN</span></div>
      @guest
      @else
      <ul class="nav navbar-nav navbar-right be-icons-nav">
              <li class="dropdown"><a><img src="{{ asset('beagle/img/Logo_PLN.png') }}" style="max-height: 50px;"></a>
              </li>
               
            </ul>
            @endguest
          </div>
        </div>
      </nav>
      @guest
      @else
      <div class="be-left-sidebar">
        <div class="left-sidebar-wrapper"><a href="#" class="left-sidebar-toggle">Dashboard</a>
          <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">

              <div class="left-sidebar-content">
                <div class="progress-widget">
                  <p class="text-center">Selamat Datang</p>
                  <p class="text-center">{{ Auth::user()->name }}<br>
                  <small>{{ Auth::user()->email }}</small></p>
                <!-- <div class="progress-data"><span class="progress-value">60%</span><span class="name">Current Project</span></div> -->
                <!-- <div class="progress"> -->
                  <!-- <div class="progress-bar progress-bar-primary" style="width: 60%;"></div> -->
                <!-- </div> -->
              </div>
                <ul class="sidebar-elements">
                  <li class="divider">Menu</li>
                  <li>
                    <a href="{{ route('home') }}">
                      <i class="icon mdi mdi-home"></i><span>Dashboard</span>
                    </a>
                  </li>
                  @if (Auth::user()->isRole('Administrator'))
                  <li>
                    <a href="{{ url ('/upload') }}">
                      <i class="icon mdi mdi-file-plus"></i><span>Upload Data</span>
                    </a>
                  </li>
                  @endif

                  <li class="parent"><a href="#"><i class="icon mdi mdi-folder"></i><span>Belum Callback</span></a>
                    <ul class="sub-menu">
                      <li class="parent"><a href="#"><i class="icon mdi mdi-undefined"></i><span>TELE Survey</span></a>
                        <ul class="sub-menu">
                          <li><a href="{{ url ('/Master/PelangganPB') }}"><i class="icon mdi mdi-undefined"></i><span>Pelanggan PB</span></a></li>
                          <li><a href="{{ url ('/Master/PelangganPD') }}"><i class="icon mdi mdi-undefined"></i><span>Pelanggan PD</span></a></li>
                          <li><a href="#"><i class="icon mdi mdi-undefined"></i><span>APKT Gangguan</span></a></li>
                          <li><a href="#"><i class="icon mdi mdi-undefined"></i><span>APKT Keluhan</span></a></li>
                          <li><a href="#"><i class="icon mdi mdi-undefined"></i><span>Survey by Request</span></a></li>
                        </ul>
                      </li>
                      <li class="parent"><a href="#"><i class="icon mdi mdi-undefined"></i><span>TELE Marketing</span></a>
                        <ul class="sub-menu">
                          <li><a href="#"><i class="icon mdi mdi-undefined"></i><span>Pelanggan DPLD MAX</span></a></li>
                          <li><a href="#"><i class="icon mdi mdi-undefined"></i><span>Program Promo</span></a></li>
                        </ul>
                      </li>
                      <li class="parent"><a href="#"><i class="icon mdi mdi-undefined"></i><span>TELE Collecting</span></a>
                        <ul class="sub-menu">
                          <li><a href="#"><i class="icon mdi mdi-undefined"></i><span>Menunggak 1-2 Lembar</span></a></li>
                          <li><a href="#"><i class="icon mdi mdi-undefined"></i><span>Menunggak Lebih 2 Lembar</span></a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>

                  <li class="parent"><a href="#"><i class="icon mdi mdi-folder-outline"></i><span>Sudah Callback</span></a>
                    <ul class="sub-menu">
                      <li class="parent"><a href="#"><i class="icon mdi mdi-undefined"></i><span>TELE Survey</span></a>
                        <ul class="sub-menu">
                          <li><a href="{{ url ('/sudah-callback-pb') }}"><i class="icon mdi mdi-undefined"></i><span>Pelanggan PB</span></a></li>
                          <li><a href="{{ url ('/sudah-callback-pd') }}"><i class="icon mdi mdi-undefined"></i><span>Pelanggan PD</span></a></li>
                          <li><a href="#"><i class="icon mdi mdi-undefined"></i><span>APKT Gangguan</span></a></li>
                          <li><a href="#"><i class="icon mdi mdi-undefined"></i><span>APKT Keluhan</span></a></li>
                          <li><a href="#"><i class="icon mdi mdi-undefined"></i><span>Survey by Request</span></a></li>
                        </ul>
                      </li>
                      <li class="parent"><a href="#"><i class="icon mdi mdi-undefined"></i><span>TELE Marketing</span></a>
                        <ul class="sub-menu">
                          <li><a href="#"><i class="icon mdi mdi-undefined"></i><span>Pelanggan DPLD MAX</span></a></li>
                          <li><a href="#"><i class="icon mdi mdi-undefined"></i><span>Program Promo</span></a></li>
                        </ul>
                      </li>
                      <li class="parent"><a href="#"><i class="icon mdi mdi-undefined"></i><span>TELE Collecting</span></a>
                        <ul class="sub-menu">
                          <li><a href="#"><i class="icon mdi mdi-undefined"></i><span>Menunggak 1-2 Lembar</span></a></li>
                          <li><a href="#"><i class="icon mdi mdi-undefined"></i><span>Menunggak Lebih 2 Lembar</span></a></li>
                        </ul>
                      </li>
                    </ul>
                  </li>

                  <li class="parent"><a href="#"><i class="icon mdi mdi-view-compact"></i><span>Rekapitulasi</span></a>
                    <ul class="sub-menu">
                      <li class="parent"><a href="#"><i class="icon mdi mdi-undefined"></i><span>Pasang Baru</span></a>
                        <ul class="sub-menu">
                          <li><a href="{{ url ('/RekapIntPb') }}">Integritas </a></li>
                          <li><a href="{{ url ('/RekapHTSPb') }}">High Trust Society </a></li>
                          <li><a href="{{ url ('/RekapKlPb') }}">Kecepatan Layanan </a></li>
                          <li><a href="{{ url ('/RekapResPb') }}">Resume </a></li>
                          <li><a href="{{ url ('/RekapATBPb') }}">Adanya Tambahan Biaya </a></li>
                          <li><a href="{{ url ('/RekapPBNPb') }}">Pelanggan Belum Nyala </a></li>
                        </ul>
                      </li>
                      <li class="parent"><a href="#"><i class="icon mdi mdi-undefined"></i><span>Perubahan Daya</span></a>
                        <ul class="sub-menu">
                          <li><a href="{{ url ('/RekapIntPd') }}">Integritas </a></li>
                          <li><a href="{{ url ('/RekapHTSPd') }}">High Trust Society </a></li>
                          <li><a href="{{ url ('/RekapKlPd') }}">Kecepatan Layanan </a></li>
                          <li><a href="{{ url ('/RekapResPd') }}">Resume </a></li>
                          <li><a href="{{ url ('/RekapATBPd') }}">Adanya Tambahan Biaya </a></li>
                          <li><a href="{{ url ('/RekapPBNPd') }}">Pelanggan Belum Nyala </a></li>
                        </ul>
                      </li>
                      <li><a href="{{ url ('/RekapKp') }}"><i class="icon mdi mdi-undefined"></i><span>Kinerja Petugas</span></a></li>
                    </ul>
                    
                  </li>
                  @if (Auth::user()->isRole('Administrator'))
                  <li class="parent"><a href="#"><i class="icon mdi mdi-grid"></i><span>Master</span></a>
                    <ul class="sub-menu">
                      <li><a href="{{ url ('/Master/User') }}">User </a></li>
                      <li><a href="{{ url ('/Master/Target') }}">Target </a></li>
                    </ul>
                  </li>
                  @endif
                 
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="be-content">
      @endguest
        <!-- <div class="main-content container-fluid"></div> -->

        <div class="main-content">
        @yield('content')
        </div>
        
      </div>
      
    </div>
    @yield('beforescript')
    <script src="{{ asset('beagle/lib/jquery/jquery.min.js') }}" type="text/javascript"></script>

    <!-- <script src="{{ asset('beagle/lib/summernote/jquery.js') }}"></script>  -->

    <script src="{{ asset('beagle/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/js/main.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/lib/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/lib/datapicker/bootstrap-datepicker.js') }}"></script>

    <script src="{{ asset('beagle/lib/jquery.sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/lib/countup/countUp.min.js') }}" type="text/javascript"></script>
    <!-- <script src="{{ asset('beagle/js/app-dashboard.js') }}" type="text/javascript"></script> -->
    <script src="{{ asset('beagle/lib/chartjs/Chart.min.js') }}" type="text/javascript"></script>


    <script src="{{ asset('beagle/lib/dataTables2/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/lib/datatables/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/lib/datatables/plugins/buttons/js/dataTables.buttons.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/lib/datatables/plugins/buttons/js/buttons.html5.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/lib/datatables/plugins/buttons/js/buttons.flash.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/lib/datatables/plugins/buttons/js/buttons.print.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/lib/datatables/plugins/buttons/js/buttons.colVis.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/lib/datatables/plugins/buttons/js/buttons.bootstrap.js') }}" type="text/javascript"></script>

    <script src="{{ asset('beagle/lib/jquery.sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/lib/countup/countUp.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('beagle/js/app-dashboard.js') }}" type="text/javascript"></script>


    <script type="text/javascript">
        $('#data_1 .input-group.date').datepicker({
                    todayBtn: "linked",
                    format: 'yyyy-mm-dd',
                    keyboardNavigation: false,
                    forceParse: false,
                    calendarWeeks: true,
                    autoclose: true
                });

      $("#table3").dataTable({
        "aaSorting": [],
        "paging": false,
        buttons: [
                    {extend: 'excel', title: 'CallBack Center PLN WILSU'},
                    {
                      extend: 'pdf', 
                      title: 'CallBack Center PLN WILSU',
                      orientation: 'landscape',
                      pageSize: 'TABLOID'
                    },
                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '8px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom:  
        "<'row'<'col-sm-5'l><'col-sm-3 text-right'f><'col-sm-4 text-right'B>>" +
              "<'row'<'col-sm-12'tr>>" +
              "<'row'<'col-sm-5'i><'col-sm-7'p>>"
      });

      $(document).ready(function(){
        //initialize the javascript
        App.init();
      });

    </script>
    @yield('afterscript')
  </body>
</html>