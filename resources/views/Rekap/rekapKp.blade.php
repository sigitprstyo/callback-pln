@extends('layouts.app')
@section('content')


    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif

     @if (\Session::has('errors'))
      <div class="alert alert-danger">
        <p>{{ \Session::get('errors') }}</p>
      </div><br />
      @endif

<div class="panel panel-default panel-border-color panel-border-color-danger">
                <div class="panel-heading panel-heading-divider">Rekapitulasi Kinerja Petugas
                <button class="btn btn-space btn-warning btn-sm pull-right"  data-toggle="modal" data-target="#filterModal"><i class="icon mdi mdi-filter-list"></i> Filter</button></div>
                <div class="panel-body">
      <div class="table-responsive">
      <table id="table3" class="table table-striped">
              <thead>
                <tr>
                  <th>Petugas</th>
                  <th>Total Pelanggan PB</th>
                  <th>Total Pelanggan PD</th>
                  <th>Total PB + PD</th>
                  <th>Target</th>
                  <th>Kontribusi PB</th>
                  <th>Kontribusi PD</th>
                  <th>Kontribusi Target PB</th>
                  <th>Kontribusi Target PD</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($cards as $key)
                <tr>
                  <td>{{ $key->name }}</td>
                  <td>{{ $key->pb }}</td>
                  <td>{{ $key->pd }}</td>
                  <td>{{ ($key->pb+$key->pd) }}</td>
                  <td>{{ $target->target_callback }}</td>
                  <td>{{ ($key->pb/$key->total)*100 }} %</td>
                  <td>{{ ($key->pd/$key->total)*100 }} %</td>
                  <td>{{ ($key->pb/$target->target_callback)*100 }} %</td>
                  <td>{{ ($key->pd/$target->target_callback)*100 }} %</td>
                </tr>
                @endforeach
              </tbody>
    
    </table>
  </div>
  </div>
</div>

<!-- Filter Modal -->
<div id="filterModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Filter </h4>
      </div>
      <div class="modal-body">
      <form method="post" name="filterPost" action="{{url($url)}}">
      {{csrf_field()}}
      <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-11">
            <label for="nama_penyulang">Bulan:</label>
            <select type="text" name="month" class="form-control">
              <option value="{{ date('m') }}">{{ date('F') }}</option>
              <option value="01">Januari</option>
              <option value="02">Februari</option>
              <option value="03">Maret</option>
              <option value="04">April</option>
              <option value="05">Mei</option>
              <option value="06">Juni</option>
              <option value="07">Juli</option>
              <option value="08">Agustus</option>
              <option value="09">September</option>
              <option value="10">Oktober</option>
              <option value="11">November</option>
              <option value="12">Desember</option>
            </select>
          </div>
        </div>

      <div class="row">
          <div class="col-lg-12 margin-tb"></div>
          <div class="form-group col-md-11">
            <label for="nama_penyulang">Tahun:</label>
            <select type="text" name="year" class="form-control">
              <option value="{{ date('Y') }}">{{ date('Y') }}</option>
              <option value="{{ date('Y', strtotime('- 1 year')) }} ">{{ date('Y', strtotime('- 1 year')) }}</option>
              <option value="{{ date('Y', strtotime('- 2 year')) }} ">{{ date('Y', strtotime('- 2 year')) }}</option>
              <option value="{{ date('Y', strtotime('- 3 year')) }} ">{{ date('Y', strtotime('- 3 year')) }}</option>
            </select>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="submit" class="btn btn-lg btn-success" value="Submit"></button>
      </div>
      </form>
    </div>
  </div>
</div>
 @endsection   